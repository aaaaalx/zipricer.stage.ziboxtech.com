<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\LinksForm;
use common\models\Links;

class ParsingController extends Controller
{
    public function actionRun(){
        Yii::$app->parser->runParser();
    }

    public function actionRunLink($url, $save = false){
        $form = new LinksForm();
        $post = [
            'LinksForm' => [
                'url' => $url,
                'status' => 1
            ]
        ];
        if($form->load($post) && $form->validate()){
            $LinksModels = Links::find()->where(['link_url'=>$url])->one();
            if(!empty($LinksModels)){
                Yii::$app->parser->runParserByUrl($url, $save);
            }
        }
        else{
            echo 'Not valid url!';
        }

    }
}