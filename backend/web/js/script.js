$(document).ready(function () {
    //get users list in popup, and set selected user id into "user_id" field
    $('#getUsers').on('click', function(e){
        var field = $(this).data('name');
        var button = $(this);
        e.preventDefault();
        $('#pModal').modal('show')
            .find('.pModal-content')
            .load($(this).data('url'), function(e){
                $(this).find('#ajaxIframe').on('load', function(){
                    var currIframe = $(this).contents();
                    currIframe.find('#selectItem').on('click', function(){
                        var user_id = currIframe.find('table input[type="radio"]:checked');
                        if(user_id.length){
                            var user = user_id.parents('tr').clone();
                            user.children()[0].remove();
                            user.children()[0].remove();
                            user = user.prop('innerHTML').replaceAll('<td>','<span>').replaceAll('</td>','</span>');
                            user = '<div class="userResult">ID: '+user+'</div>';
                            user_id = user_id.val();
                            $('input[name="'+field+'"]').val(user_id);
                            var userConteiner = button.parent().find('.userResult');
                            if(userConteiner.length){
                                userConteiner.remove()
                            }
                            button.parent().append(user);
                            $('#pModal').modal('toggle');
                        }
                        else{
                            $('input[name="'+field+'"]').val('');
                            $('#pModal').modal('toggle');
                        }
                    });
                });
            });
    });

    //select image
    $(document).on('click', '.fileNameButton', function () {
        var target = $(this).data('filename');
        if(target){
            var targetField = $('input[name="'+target+'"]');
            if(targetField.length){
                targetField.click();
            }
        }
    });
    //on image selected insert preview
    $(document).on('change', '.fileInputField', function () {
        var name = $(this).attr('name');
        var button = $('[data-filename="'+name+'"]');
        var imageContainer = $(button).parent().find('.image-container');
        previewFile(this, imageContainer, name);
    });
    //remove image
    $(document).on('click', '.close-img-button', function(){
        var field = $('input[type="hidden"][name="'+$(this).data('filename')+'"]');
        if(field.length){
            field.val('delete');
            $(this).parent().html('');
        }
    });

    //add dynamic field
    $(document).on('click', '.addField', function(){
        var fieldGroup = $(this).parent().find('.dynamic-form-group').eq(0).clone();
        var className = fieldGroup.data('class');
        var fieldInput = fieldGroup.find('input').not('[type="hidden"]');
        var fieldSelect = fieldGroup.find('select');
        var container = $(this).parent().find('.fields-container');
        var itemsCount = container.children().length-1;
        fieldInput.val('');
        fieldSelect.each(function(k, v) {
            $(this).attr('value', $(this).children().eq(0).val());
        });
        var fields = fieldGroup.find('.dynamicField');
        if(fields.length){
            ++itemsCount;
            fields.each(function (k,v) {
                var itemName = className+'['+(itemsCount)+']['+$(v).data('name')+']';
                $(v).attr('id', itemName);
                $(v).attr('name', itemName);
                $(v).prev('label').attr('for', itemName);
            })
        }
        fieldGroup.find('.help-block').html('');
        container.append(fieldGroup);
    });
    //remove dynamic field
    $(document).on('click', '.fieldRemoveButton', function(){
        var fieldsCount = $(this).parents('.fields-container').children().length;
        if(fieldsCount>1){
            $(this).parent().remove();
        }
        else{
            var field = $(this).parent().find('input');
            field.val('');
            field.trigger('change');
        }
    });

    //change status
    $(document).on('change', '.changeStatus', function(){
        var id = $(this).data('id');
        var project_id = $(this).data('project_id');
        var status = $(this).val();
        var csrf = $(this).data('csrf');
        console.log(status);

        $.ajax({
            url: '/projects-links/change-status',
            type: 'post',
            data: {
                id: id,
                status: status,
                project_id: project_id,
                _csrf : csrf
            },
            success: function (data) {
                if(data){}
            }
        });
    });

});

//make image preview
function previewFile(file,imageContainer, name) {
    if (file.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var img = $(document.createElement('img'));
            img.attr('src', e.target.result);
            imageContainer.html('');
            imageContainer.append(img);
            img.parent().append('<div data-filename="'+name+'" class="close-img-button"><span class="fa fa-times-circle"></span></div>')
        };
        reader.readAsDataURL(file.files[0]);
    }
}

//add replace all in string function
String.prototype.replaceAll = function(target, replacement) {
    return this.split(target).join(replacement);
};