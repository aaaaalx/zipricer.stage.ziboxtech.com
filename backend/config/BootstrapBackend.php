<?php

namespace backend\config;

use Yii;
use yii\base\BootstrapInterface;
use core\interfaces\ProjectRepositoryInterface;
use core\repositories\ProjectRepository;
use core\repositories\NotificationRepository;
use core\interfaces\NotificationRepositoryInterface;

class BootstrapBackend implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $conteiner = Yii::$container;
        $conteiner->setSingleton(ProjectRepositoryInterface::class, ProjectRepository::class);
        $conteiner->setSingleton(NotificationRepositoryInterface::class, NotificationRepository::class);
    }
}