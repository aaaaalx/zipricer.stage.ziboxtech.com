<?php
use yii\helpers\Url;
?>
<section class="content">

    <div class="body-content">
        <div class="row">

            <div class="col-lg-4">
                <h2>Parsing</h2>

                <p><a class="btn btn-default" href="<?= Url::to(['parser/run']) ?>">Run parsing</a></p>
            </div>

        </div>
    </div>

</section>
