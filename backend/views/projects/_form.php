<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Projects;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */
/* @var $form yii\widgets\ActiveForm */

/*высчитывать разницу между двумя timestamp
$today = strtotime('2018-08-27 00:00:00');
$yesterday = strtotime('2018-08-26 02:59:59');
print_r(date('Y-m-d H:i:s', $today));
echo '<br>';
print_r(date('Y-m-d H:i:s', $yesterday));
echo '<br>';
var_dump(round(($today-$yesterday)/3600, 2));//in hours
die;
*/
?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'photo')->fileInput(['class'=>'hiddenEl fileInputField']) ?>

    <div class="file-input-content">
        <?= Html::button(Yii::t('app', 'Add photo'), ['data-filename'=>'Projects[photo]', 'class' => 'fileNameButton btn btn-success']) ?>
        <div class="image-container">
            <?php
            if($model->photo){
                echo '<img src="'.$model->getUploadsUrl().$model->photo.'"><div data-filename="Projects[photo]" class="close-img-button"><span class="fa fa-times-circle"></span></div>';
            }
            ?>
        </div>
    </div>

    <?= $form->field($model, 'vendor_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->hiddenInput() ?>

    <div class="form-group">
        <?= Html::button(Yii::t('app', 'Add user'), [
            'data-name'=>'user_id',
            'id'=>'getUsers',
            'class'=>'btn btn-success',
            'data-url'=>Url::to(['projects/get-iframe',
            'url'=>base64_encode('/projects/get-users'), 'layout'=>'mainBlank'])]) ?>
        <div class="userResult">
            <?php if($model->user): ?>
            ID: <span><?= $model->user->attributes['id'] ?></span><span><?= $model->user->attributes['username'] ?></span><span><?= $model->user->attributes['email'] ?></span><span><?= date('Y-m-d H:i:s', $model->user->attributes['created_at']) ?></span><span><?= date('Y-m-d H:i:s', $model->user->attributes['updated_at']) ?></span>
            <?php endif; ?>
        </div>
    </div>

    <?= $form->field($model, 'status')->dropDownList(Projects::getStatuses()) ?>

    <div class="form-group">
        <label class="control-label"><?= Yii::t('app', 'Urls') ?></label>
        <div class="fields-container">
            <div class="form-group">
                <?= Html::textInput('DynamicFields_url[]', '', ['class' => 'dynamicField form-control']) ?>
                <span class="fieldRemoveButton fa fa-close"></span>
            </div>
        </div>
        <?= Html::button(Yii::t('app', 'Add url'), ['class' => 'addField btn btn-success']) ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php yii\bootstrap\Modal::begin([
            'id'=>'pModal',
            'header'=>'<h4>'.Yii::t('app', 'Add user').'</h4>',
            'size'=>'modal-lg',
    ]); ?>
    <div class="pModal-content"></div>
    <?php yii\bootstrap\Modal::end(); ?>

</div>
