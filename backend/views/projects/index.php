<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Projects', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'description',
            'vendor_code',
            [
                'label' => Yii::t('app', 'Photo'),
                'format' => 'html',
                'value' => function($model) {
                    return $model->photo? Html::img($model->getUploadsUrl($model->id, $model->photo), ['style' => 'max-width: 150px;']) : Yii::t('app', 'No photo');
                }
            ],
            [
                'label' => Yii::t('app','Links'),
                'attribute' => 'links',
                'format' => 'raw',
                'value'=>function ($data) {
                    return count($data->links);
                },
            ],
            'user_id',
            [
                'attribute' => 'status',
                'label' => Yii::t('app','Status'),
                'format' => 'raw',
                'filter' => $searchModel->getStatuses(),
                'value'=>function ($data) {
                    $statusArr = $data->getStatuses();
                    if(isset($statusArr[$data->status])){
                        return Html::a($statusArr[$data->status], ['/projects/change-status', 'id'=>$data->id]);
                    }
                    return Yii::t('app','Status not set');
                },
            ],
            [
                'attribute' => 'created_at',
                'label' => Yii::t('app','Created'),
                'value'=>function ($data) {
                    return $date = date('Y-m-d H:i:s', $data->created_at);
                },
            ],
            [
                'attribute' => 'updated_at',
                'label' => Yii::t('app','Updated'),
                'value'=>function ($data) {
                    return $date = date('Y-m-d H:i:s', $data->updated_at);
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' =>'{view} {update} {delete} {links}',
                'buttons' => [
                    'links' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> ', ['projects-links/index', 'project_id' => $model->id], ['title' => Yii::t('app','Links')]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
