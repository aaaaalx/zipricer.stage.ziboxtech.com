<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\Projects;

/* @var $this yii\web\View */
/* @var $projectsForm core\forms\ProjectsForm */
/* @var $project core\entity\Project */

$this->title = 'Create Projects';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($projectsForm, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($projectsForm, 'description')->textarea(['maxlength' => true]) ?>

    <?= $form->field($projectsForm, 'photo')->fileInput(['class'=>'hiddenEl fileInputField']) ?>

    <div class="file-input-content">
        <?= Html::button(Yii::t('app', 'Add photo'), ['data-filename'=>'ProjectsForm[photo]', 'class' => 'fileNameButton btn btn-success']) ?>
        <div class="image-container">
            <?php
            if($projectsForm->photo){
                echo '<img src="'.$project->getUploadsUrl($project->id, $projectsForm->photo).'"><div data-filename="ProjectsForm[photo]" class="close-img-button"><span class="fa fa-times-circle"></span></div>';
            }
            ?>
        </div>
    </div>

    <?= $form->field($projectsForm, 'vendor_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($projectsForm, 'price')->textInput(['maxlength' => true, 'type'=>'number']) ?>

    <?= $form->field($projectsForm, 'user_id')->hiddenInput() ?>

    <div class="form-group">
        <?= Html::button(Yii::t('app', 'Add user'), [
            'data-name'=>'ProjectsForm[user_id]',
            'id'=>'getUsers',
            'class'=>'btn btn-success',
            'data-url'=>Url::to(['projects/get-iframe',
                'url'=>base64_encode('/projects/get-users'), 'layout'=>'mainBlank'])]) ?>
        <div class="userResult">
            <?php if($project->user): ?>
                ID: <span><?= $project->user->attributes['id'] ?></span><span><?= $project->user->attributes['username'] ?></span><span><?= $project->user->attributes['email'] ?></span><span><?= date('Y-m-d H:i:s', $project->user->attributes['created_at']) ?></span><span><?= date('Y-m-d H:i:s', $project->user->attributes['updated_at']) ?></span>
            <?php endif; ?>
        </div>
    </div>

    <?= $form->field($projectsForm, 'status')->dropDownList(Projects::getStatuses()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php yii\bootstrap\Modal::begin([
        'id'=>'pModal',
        'header'=>'<h4>'.Yii::t('app', 'Add user').'</h4>',
        'size'=>'modal-lg',
    ]); ?>
    <div class="pModal-content"></div>
    <?php yii\bootstrap\Modal::end(); ?>

</div>
