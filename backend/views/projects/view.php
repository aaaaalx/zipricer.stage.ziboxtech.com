<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Projects */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'label' => Yii::t('app', 'Photo'),
                'format' => 'html',
                'value' => function($model) {
                    return $model->photo? Html::img($model->getUploadsUrl(), ['class' => 'maxWidth']) : Yii::t('app', 'No photo');
                }
            ],
            'description',
            'vendor_code',
            'user_id',
            [
                'attribute' => 'status',
                'label' => Yii::t('app','Status'),
                'format' => 'raw',
                'filter' => $model->getStatuses(),
                'value'=>function ($data) {
                    $statusArr = $data->getStatuses();
                    if(isset($statusArr[$data->status])){
                        return $statusArr[$data->status];
                    }
                    return Yii::t('app','Status not set');
                },
            ],
            [
                'attribute' => 'created_at',
                'label' => Yii::t('app','Created'),
                'value'=>function ($data) {
                    return $date = date('Y-m-d H:i:s', $data->created_at);
                },
            ],
            [
                'attribute' => 'updated_at',
                'label' => Yii::t('app','Updated'),
                'value'=>function ($data) {
                    return $date = date('Y-m-d H:i:s', $data->updated_at);
                },
            ],
        ],
    ]) ?>

</div>
