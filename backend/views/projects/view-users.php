<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\RadioButtonColumn'],
            'id',
            [
                'attribute' => 'username',
                'label' => Yii::t('app','User name'),
                'value'=>function ($data) {
                    return $data->username;
                },
            ],
            'email',
            [
                'attribute' => 'created_at',
                'label' => Yii::t('app','Created'),
                'value'=>function ($data) {
                    return $date = date('Y-m-d H:i:s', $data->created_at);
                },
            ],
            [
                'attribute' => 'updated_at',
                'label' => Yii::t('app','Updated'),
                'value'=>function ($data) {
                    return $date = date('Y-m-d H:i:s', $data->updated_at);
                },
            ]
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::button('Select', ['class' => 'btn btn-success', 'id'=>'selectItem']) ?>
    </div>

</div>
