<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-index">

    <h1><?= Yii::t('app','Список Админов'); ?></h1>

    <p>
        <?= Html::a('Create Admin', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'username',
                'label' => Yii::t('app','User name'),
                'value'=>function ($data) {
                    return $data->username;
                },
            ],
            'email',
            [
                'attribute' => 'created_at',
                'label' => Yii::t('app','Created'),
                'value'=>function ($data) {
                    return $date = date('Y-m-d H:i:s', $data->created_at);
                },
            ],
            [
                'attribute' => 'updated_at',
                'label' => Yii::t('app','Updated'),
                'value'=>function ($data) {
                    return $date = date('Y-m-d H:i:s', $data->updated_at);
                },
            ],
            [
                'attribute' => 'status',
                'label' => Yii::t('app','Status'),
                'format' => 'raw',
                'value'=>function ($data) {
                    $status = $data->status>0?Yii::t('app','active'):Yii::t('app','not active');
                    return Html::a($status, ['/admin/change-status', 'id'=>$data->id]);
                },
            ],
            [
                'label' => Yii::t('app','Edit'),
                'format' => 'raw',
                'value'=>function ($data) {
                    return Html::a(Yii::t('app','edit'), ['/admin/update', 'id'=>$data->id]);
                },
            ],
            [
                'label' => Yii::t('app','Delete'),
                'format' => 'raw',
                'value'=>function ($data) {
                    return Html::a(Yii::t('app','delete'), ['/admin/delete', 'id'=>$data->id]);
                },
            ],

        ],
    ]); ?>
</div>
