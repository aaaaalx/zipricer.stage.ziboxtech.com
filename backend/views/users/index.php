<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Yii::t('app','Список Пльзователей'); ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'username',
                'label' => Yii::t('app','User name'),
                'value'=>function ($data) {
                    return $data->username;
                },
            ],
            [
                'attribute' => 'added_on',
                'label' => Yii::t('app','Created at'),
                'value'=>function ($data) {
                    return $data->added_on;
                },
            ],

        ],
    ]); ?>
</div>
