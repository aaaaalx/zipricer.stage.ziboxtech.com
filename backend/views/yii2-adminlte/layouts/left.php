<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <!--
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        -->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => Yii::t('app', 'Main menu'), 'options' => ['class' => 'header']],
                    ['label' => Yii::t('app', 'Login'), 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => Yii::t('app', 'Users'),
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('app', 'Profiles list'), 'icon' => 'file-code-o', 'url' => ['/profile/index'],],
                            ['label' => Yii::t('app', 'Users list'), 'icon' => 'file-code-o', 'url' => ['/users/index'],],
                            ['label' => Yii::t('app', 'Admins list'), 'icon' => 'file-code-o', 'url' => ['/admin/index'],],
                            /*
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        */
                        ],
                    ],
                    ['label' => Yii::t('app', 'Projects'), 'icon' => 'file-code-o', 'url' => ['/projects/index'],],
                    ['label' => Yii::t('app', 'Notifications'), 'icon' => 'file-code-o', 'url' => ['/notifications/index'],],
                    ['label' => Yii::t('app', 'Parser'), 'icon' => 'file-code-o', 'url' => ['/parser/index'],],
                ],
            ]
        ) ?>

    </section>

</aside>
