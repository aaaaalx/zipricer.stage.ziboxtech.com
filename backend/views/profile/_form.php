<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Profile;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'referral_user_id')->textInput() ?>
    <?= $form->field($model, 'referral_link')->textInput() ?>
    <?= $form->field($model, 'status')->dropDownList(Profile::getStatuses()) ?>

    <?= $form->field($model, 'icon')->fileInput(['class'=>'hiddenEl fileInputField']) ?>
    <div class="file-input-content">
        <?= Html::button(Yii::t('app', 'Add photo'), ['data-filename'=>'ProjectsForm[photo]', 'class' => 'fileNameButton btn btn-success']) ?>
        <div class="image-container"></div>
    </div>

    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'password_repeat')->passwordInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
