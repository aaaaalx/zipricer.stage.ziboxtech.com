<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NotificationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Notifications');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="notifications-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Notifications'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'message',
            'status',
            [
                'attribute' => 'publication_at',
                'label' => Yii::t('app','Publication'),
                'format' => ['date','Y-m-dd HH:i:s']
            ],
            [
                'attribute' => 'created_at',
                'label' => Yii::t('app','Created'),
                'format' => ['date','Y-m-dd HH:i:s']
            ],
            [
                'attribute' => 'updated_at',
                'label' => Yii::t('app','Updated'),
                'format' => ['date','Y-m-dd HH:i:s']
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
