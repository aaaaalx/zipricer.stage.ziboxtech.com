<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use core\entity\Notification;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $notificationForm core\forms\NotificationForm */

$this->title = Yii::t('app', 'Create Notification');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notification'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$url = Url::to(['notifications/get-users']);

?>
<div class="notifications-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="notifications-form">

        <?php $form = ActiveForm::begin([
                //'enableAjaxValidation' => true,
                //'validationUrl' => Url::toRoute('notifications/validation')
                //'options'=>[
                //    'id'=>basename(get_class($notificationForm)).'_form'
                //]
        ]); ?>

        <?= $form->field($notificationForm, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($notificationForm, 'message')->textarea(['maxlength' => true]) ?>

        <?= $form->field($notificationForm, 'status')->dropDownList(Notification::getStatuses()) ?>

        <?= $form->field($notificationForm, 'publication_at')->widget(DateTimePicker::class,[
            'options' => ['placeholder' => Yii::t('app', 'Select publication date and time')],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd hh:ii'
            ]
        ]) ?>

        <?= $form->field($notificationForm, 'users')->widget(Select2::class, [
        'language' => Yii::$app->language,
        'name' => 'users',
        'maintainOrder' => false,
        'toggleAllSettings' => [
            'selectLabel' => '',
            'unselectLabel' => '',
        ],
        'options' => [
            'placeholder' => Yii::t('app', 'Add users to send notify'),
            'multiple' => true
        ],
        'pluginOptions' => [
            'allowClear' => false,
            'minimumInputLength' => 0,
            'maximumInputLength' => 20,
            'tags' => false,
            'tokenSeparators' => [','],
            'language' => [
                'errorLoading' => new JsExpression("function () { return '".Yii::t('app', 'Waiting for results...')."'; }"),
            ],
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(result) { return result.text; }'),
            'templateSelection' => new JsExpression('function (result) { return result.text; }'),
        ],
        'pluginEvents' => [
                "change" => "function() {}",
                //"select2:opening" => "function() { console.log(this); }",
                //"select2:open" => "function() { console.log('open'); }",
                //"select2:closing" => "function() { console.log('close'); }",
                //"select2:close" => "function() { console.log('close'); }",
                //"select2:selecting" => "function() { console.log('selecting'); }",
                "select2:select" => "function() { 
                    if($(this).children('[value=\"0\"]').length){
                        $(this).children(':not([value=\"0\"])').remove();
                    }
                }",
                //"select2:unselecting" => "function() { console.log('unselecting'); }",
                "select2:unselect" => "function() {
                    if($(this).children('[value=\"0\"]').length){
                        $(this).children('[value=\"0\"]').remove();
                    }
                }"
            ]

        ]) ?>

        <?= $form->errorSummary($notificationForm); ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
