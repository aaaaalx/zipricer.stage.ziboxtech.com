<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ProjectsLinks;

/* @var $this yii\web\View */
/* @var $model common\models\ProjectsLinks */
/* @var $form yii\widgets\ActiveForm */

$class = 'LinksForm';

$this->title = Yii::t('app', 'Create Projects Links');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-links-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="projects-links-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group">
            <div class="fields-container">

                <?php foreach ($links as $index => $link): ?>
                    <div data-class="<?= $class; ?>" class="dynamic-form-group">
                        <?= $form->field($link, "[$index]url")->textInput(['data-name'=>'url', 'class'=>'dynamicField form-control', 'id'=>$class."[$index][url]"]) ?>
                        <?= $form->field($link, "[$index]status")->dropDownList(ProjectsLinks::getStatuses(),['data-name'=>'status', 'class'=>'dynamicField form-control', 'id'=>$class."[$index][status]"])->label('') ?>
                        <?= $form->field($link, "[$index]project_id")->hiddenInput(['data-name'=>'project_id', 'class'=>'dynamicField form-control', 'value'=>$project_id, 'id'=>$class."[$index][project_id]"])->label('') ?>
                        <span class="fieldRemoveButton fa fa-close"></span>
                    </div>
                <?php endforeach; ?>

            </div>
            <?= Html::button(Yii::t('app', 'Add url'), ['class' => 'addField btn btn-success']) ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
