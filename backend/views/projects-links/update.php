<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ProjectsLinks;

/* @var $this yii\web\View */
/* @var $model common\models\ProjectsLinks */

$this->title = Yii::t('app', 'Update Projects Links: ' . $model->id, [
    'nameAttribute' => '' . $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="projects-links-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="projects-links-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($linksForm, "url")->textInput() ?>
        <?= $form->field($linksForm, "status")->dropDownList(ProjectsLinks::getStatuses()) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
