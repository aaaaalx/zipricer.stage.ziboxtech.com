<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use common\models\Links;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProjectsLinksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects Links').' of '.Yii::t('app', 'Project').': '.$project_id;
$this->params['breadcrumbs'][] = $this->title;

$linksStatuses = Links::getStatuses();
?>
<div class="projects-links-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Projects Links'), ['create', 'project_id'=>$project_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => Yii::t('app','Links'),
                'attribute' => 'link',
                'value' => 'link.link_url'
            ],
            'link_id',
            [
                'label' => Yii::t('app', "Created"),
                'attribute' => 'created_at',
                'format' => ['date', 'php:Y-m-d H:i:s'],
                'filter' => DateRangePicker::widget([
                    'model'=>$searchModel,
                    'language' => Yii::$app->language,
                    'attribute'=>'createTimeRange',
                    'convertFormat'=>true,
                    'pluginOptions'=>[
                        'timePicker'=>true,
                        'timePicker24Hour' => true,
                        'timePickerIncrement'=>1,
                        'locale'=>[
                            'format'=>'Y-m-d H:i:s'
                        ]
                    ]
                ])
            ],
            [
                'label' => Yii::t('app', "Updated"),
                'attribute' => 'updated_at',
                'format' => ['date', 'php:Y-m-d H:i:s'],
                'filter' => DateRangePicker::widget([
                    'model'=>$searchModel,
                    'language' => Yii::$app->language,
                    'attribute'=>'updateTimeRange',
                    'convertFormat'=>true,
                    'value' => date('Y-m-d H:i:s', strtotime('now -1 month')).' - '. date('Y-m-d H:i:s', strtotime('now +1 month')),
                    'pluginOptions'=>[
                        'timePicker'=>true,
                        'timePicker24Hour' => true,
                        'timePickerIncrement'=>1,
                        'locale'=>[
                            'format'=>'Y-m-d H:i:s'
                        ]
                    ]
                ])
            ],
            [
                'attribute' => 'link_status',
                'label' => Yii::t('app','Link status'),
                'format' => 'raw',
                'filter' => $searchModel->getStatuses(),
                'value'=> function ($data) use ($project_id) {
                    $statusArr = $data->getStatuses();
                    return Html::dropDownList(
                        'link_status',
                        isset($statusArr[$data->link_status])?$data->link_status:null,
                        $statusArr,
                        [
                            'data-csrf'=>Yii::$app->getRequest()->getCsrfToken(),
                            'data-id'=>$data->id,
                            'data-project_id'=>$project_id,
                            'class'=>'form-control changeStatus'
                        ]);
                },
            ],
            [
                'attribute' => 'availability',
                'label' => Yii::t('app','Availability'),
                'format' => 'raw',
                'filter' => $linksStatuses ,
                'value'=> function ($data) use ($linksStatuses){
                    return isset($linksStatuses[$data->link->status])?$linksStatuses[$data->link->status]:null;
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' =>'{delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key)use($project_id) {
                        return Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> ', ['projects-links/delete', 'id'=>$model->id, 'project_id'=>$project_id], ['title'=>Yii::t('app','Delete'), 'data-method'=>"post"]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
