<?php

namespace backend\controllers;

use Yii;
use common\models\Admin;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\AdminSignupForm;
use yii\filters\AccessControl;
use common\models\AdminSearch;

/**
 * AdminController implements the CRUD actions for Admin model.
 */
class AdminController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Admin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Displays a single Admin model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Admin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AdminSignupForm();
        $model->setScenario('adminCreate');
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($user = $model->signup()) {
                Yii::$app->session->setFlash('success', "New user created!");
                return $this->redirect('/admin/index');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Admin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $admin = Admin::findIdentity($id);
        if (!$admin) {
            Yii::$app->session->setFlash('error', "Admin not exist!");
            return $this->redirect('/admin/index');
        }
        $model = new AdminSignupForm($admin);
        if ($model->load(Yii::$app->request->post()) && $model->validate($model)) {
            $admin->edit($model->username, $model->email);
            if($model->password){
                $admin->setPassword($model->password);
            }
            if ($admin->save()) {
                return $this->redirect('/admin/index');
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Admin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if($this->findModel($id)->delete()){
            \Yii::$app->getSession()->setFlash('success', Yii::t('app','Админ успешно удален!'));
        }
        else{
            \Yii::$app->getSession()->setFlash('error', 'Возникли проблемы при удалении!');
        }

        return $this->redirect(['/admin/index']);
    }

    public function actionChangeStatus($id){
        $model = Admin::findOne($id);
        $count = Admin::find()->where(['status' =>Admin::STATUS_ACTIVE])->count();
        $save = false;
        if($model){
            if($model->status == $model::STATUS_ACTIVE && $count>1){
                $model->status = $model::STATUS_DELETED;
                $save = true;
            }
            elseif($model->status != $model::STATUS_ACTIVE ){
                $model->status = $model::STATUS_ACTIVE;
                $save = true;
            }
            if($save && $model->save()){
                \Yii::$app->getSession()->setFlash('success', 'Статус обновлен!');
                return $this->redirect('/admin/index');
            }
        }
        \Yii::$app->getSession()->setFlash('error', 'Не удалось обновить статус!');
        return $this->redirect('/admin/index');
    }

    /**
     * Finds the Admin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Admin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Admin::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
