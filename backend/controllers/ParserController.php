<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use common\models\LinksForm;
use common\models\Links;
use toriphes\console\Runner;

/**
 * Parser controller
 */
class ParserController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        return $this->render('index');
    }

    public function create(){
        $links = [new LinksForm()];
        $count = count(Yii::$app->request->post('LinksForm', []));
        for ($i = 1; $i < $count; $i++){
            $links[] = new LinksForm();
        }
        if(Model::loadMultiple($links, Yii::$app->request->post()) && Model::validateMultiple($links)){
            $domains = BaseParser::getDomains();
            $urls = array_column($links, 'url');
            $LinksModels = array_column(Links::find()->where(['in', 'link_url', $urls])->all(), null, 'id');
            $existingLinks = array_column($LinksModels,'link_url', 'id');
            $errors = [
                'domains'=>[],
                'links'=>[],
                'projectLinks'=>[]
            ];
            foreach ($links as $model){
                $url = parse_url($model->url);
                $domain = @$url['host'];
                if(!in_array($domain, $domains)){
                    //отправить запрос если нет домена в списке
                    $errors['domains'][] = Yii::t('app', "$domain is not supported!");
                }
                else{
                    if($id = array_search($model->url, $existingLinks)){
                        $ProjectsLinks = new ProjectsLinks();
                        $ProjectsLinks->link_id = $id;
                        $ProjectsLinks->project_id = $project_id;
                        $ProjectsLinks->link_status = $model->status;
                        if(!$ProjectsLinks->save()){
                            $errors['projectLinks'][] = Yii::t('app', "$model->url was not added to project!").' ('.$ProjectsLinks->errors['link_id'][0].')';
                        }
                    }
                    else{
                        $Links = new Links();
                        $Links->link_url = $model->url;
                        $Links->status = 1;
                        if($Links->save()){
                            $ProjectsLinks = new ProjectsLinks();
                            $ProjectsLinks->link_id = $Links->id;
                            $ProjectsLinks->project_id = $project_id;
                            $ProjectsLinks->link_status = $model->status;
                            if(!$ProjectsLinks->save()){
                                $errors['projectLinks'][] = Yii::t('app', "$model->url was not added to project!").' ('.$ProjectsLinks->errors['link_id'][0].')';
                            }
                        }
                        else{
                            $errors['links'][] = Yii::t('app', "$model->url was not created!");
                        }
                    }
                }
            }
            if(!empty($errors['domains']) || !empty($errors['links']) || !empty($errors['projectLinks'])){
                $message = implode(' <br>', $errors['domains']).implode(' <br>', $errors['links']).implode(' <br>', $errors['projectLinks']);
                \Yii::$app->getSession()->setFlash('error',  $message);
                if(count($urls)>count($errors['domains'])&&count($urls)>count($errors['links'])&&count($urls)>count($errors['projectLinks'])){
                    \Yii::$app->getSession()->setFlash('success',  Yii::t('app', 'All rest valid links was added!'));
                }
            }
            else{
                \Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Links was successfully saved!'));
            }
            return $this->redirect(['projects-links/index', 'project_id' => $project_id]);
        }

        return $this->render('create', [
            'links' => $links,
            'project_id' => $project_id
        ]);
    }

    public function actionRun(){
//      $consoleController = new \yii\console\controllers\SuggestionController;
//      $consoleController->runAction('php yii parsing/run');

        //$this->redirect(['parser/index']);
    }

}