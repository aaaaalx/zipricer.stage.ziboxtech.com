<?php

namespace backend\controllers;

use common\models\Profile;
use Yii;
use core\forms\NotificationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use core\forms\NotificationForm;
use core\entity\Notification;
use core\services\NotificationService;
use yii\base\Module;
use yii\bootstrap\ActiveForm;
use yii\web\Response;
use yii\db\Query;

/**
 * NotificationsController implements the CRUD actions for Notifications model.
 */
class NotificationsController extends Controller
{

    private $service;

    public function __construct($id, Module $module, array $config = [], NotificationService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Notifications models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotificationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        //$this->addNotification('Привет', 'Сообщение', 8);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notifications model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionGetUsers($q = null) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, username AS text')
                ->from('profile')
                ->where(['status'=>Profile::STATUS_ACTIVE])
                ->andWhere(['id'=>$q])
                ->orWhere(['like', 'username', $q])
                ->orderBy(['id' => SORT_DESC])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $selectAll = ['id' => 0, 'text' => '<b> -- '.Yii::t('app', 'Select all').' -- </b>'];
            array_unshift($data, $selectAll);
            $out['results'] = $data;
        }
        return $out;
    }

    public function actionValidation(){
        $notificationForm = new NotificationForm();
        if(Yii::$app->request->isAjax && $notificationForm->load(Yii::$app->request->post())){
            Yii::$app->response->format = 'json';
            return ActiveForm::validate($notificationForm);
        }
        return false;
    }

    /**
     * Creates a new Notifications model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $notificationForm = new NotificationForm();
        if ($notificationForm->load(Yii::$app->request->post()) && $notificationForm->validate()) {
            $notification = $this->service->create($notificationForm);
            return $this->redirect(['view', 'id' => $notification->id]);
        }

        return $this->render('create', [
            'notificationForm' => $notificationForm,
        ]);
    }

    /**
     * Updates an existing Notification model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $notification = $this->findModel($id);
        $notificationForm = new NotificationForm($notification);
        if ($notificationForm->load(Yii::$app->request->post()) && $notificationForm->validate()) {
            $notification = $this->service->edit($notificationForm, $notification->id);
            return $this->redirect(['view', 'id' => $notification->id]);
        }
        $users = $notificationForm->users;
        $notificationForm->users = array_keys($notificationForm->users);

        return $this->render('update', [
            'notificationForm' => $notificationForm,
            'users' => $users,
            'notification' =>$notification
        ]);
    }

    public function addNotification($title, $message, $user_id){
        $notificationForm = new NotificationForm();
        $notificationForm->title = $title;
        $notificationForm->message = $message;
        $notificationForm->status = Notification::ACTIVE;
        $notificationForm->users = is_array($user_id)?$user_id:[$user_id];
        $this->service->create($notificationForm);
    }

    /**
     * Deletes an existing Notifications model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Notifications model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notification::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
