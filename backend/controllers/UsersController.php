<?php

namespace backend\controllers;

use Yii;
use common\models\Users;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\SignupForm;
use common\models\UsersSearch;
use yii\filters\AccessControl;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignupForm();
        $model->setScenario('userCreate');
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                Yii::$app->session->setFlash('success', "New user created!");
                return $this->redirect('/users/index');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $user = Users::findIdentity($id);

        if (!$user) {
            Yii::$app->session->setFlash('error', "User not exist!");
            return $this->redirect('/users/index');
        }
        $model = new SignupForm($user);
        if ($model->load(Yii::$app->request->post()) && $model->validate($model)) {
            $user->edit($model->username, $model->email);
            if($model->password){
                $user->setPassword($model->password);
            }
            if ($user->save()) {
                return $this->redirect('/user/index');
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if($this->findModel($id)->delete()){
            \Yii::$app->getSession()->setFlash('success', Yii::t('app','Пользователь успешно удален!'));
        }
        else{
            \Yii::$app->getSession()->setFlash('error', 'Возникли проблемы при удалении!');
        }

        return $this->redirect(['/users/index']);
    }

    public function actionChangeStatus($id){
        $model = Users::findOne($id);
        if($model){
            if($model->status == $model::STATUS_ACTIVE){
                $model->status = $model::STATUS_DELETED;
            }
            else{
                $model->status = $model::STATUS_ACTIVE;
            }
            if($model->update()){
                \Yii::$app->getSession()->setFlash('success', 'Статус обновлен!');
                return $this->redirect('/users/index');
            }
        }
        \Yii::$app->getSession()->setFlash('error', 'Не удалось обновить статус!');
        return $this->redirect('/users/index');
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
