<?php

namespace backend\controllers;

use Yii;
use common\models\ProjectsLinks;
use common\models\ProjectsLinksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\LinksForm;
use common\models\Links;
use common\components\parser\classes\BaseParser;
use yii\base\Model;
use yii\filters\AccessControl;

/**
 * ProjectsLinksController implements the CRUD actions for ProjectsLinks model.
 */
class ProjectsLinksController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all ProjectsLinks models.
     * @param mixed $project_id
     * @return mixed
     */
    public function actionIndex($project_id=null)
    {
        if(!$project_id){
            $this->redirect(['/projects/index']);
        }
        $searchModel = new ProjectsLinksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $project_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'project_id' => $project_id
        ]);
    }

    /**
     * Displays a single ProjectsLinks model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProjectsLinks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param mixed $project_id
     * @return mixed
     */
    public function actionCreate($project_id=null){
        if(!$project_id){
            $this->redirect(['/projects-links/index']);
        }
        $links = [new LinksForm()];
        $count = count(Yii::$app->request->post('LinksForm', []));
        for ($i = 1; $i < $count; $i++) {
            $links[] = new LinksForm();
        }
        if(Model::loadMultiple($links, Yii::$app->request->post()) && Model::validateMultiple($links)){
            $domains = BaseParser::getDomains();
            $urls = array_column($links, 'url');
            $LinksModels = array_column(Links::find()->where(['in', 'link_url', $urls])->all(), null, 'id');
            $existingLinks = array_column($LinksModels,'link_url', 'id');
            $errors = [
                'domains'=>[],
                'links'=>[],
                'projectLinks'=>[]
            ];
            foreach ($links as $model){
                $url = parse_url($model->url);
                $domain = @$url['host'];
                if(!in_array($domain, $domains)){
                    //отправить запрос если нет домена в списке
                    $errors['domains'][] = Yii::t('app', "$domain is not supported!");
                }
                else{
                    if($id = array_search($model->url, $existingLinks)){
                        $ProjectsLinks = new ProjectsLinks();
                        $ProjectsLinks->link_id = $id;
                        $ProjectsLinks->project_id = $project_id;
                        $ProjectsLinks->link_status = $model->status;
                        if(!$ProjectsLinks->save()){
                            $errors['projectLinks'][] = Yii::t('app', "$model->url was not added to project!").' ('.$ProjectsLinks->errors['link_id'][0].')';
                        }
                    }
                    else{
                        $Links = new Links();
                        $Links->link_url = $model->url;
                        $Links->status = 1;
                        if($Links->save()){
                            $ProjectsLinks = new ProjectsLinks();
                            $ProjectsLinks->link_id = $Links->id;
                            $ProjectsLinks->project_id = $project_id;
                            $ProjectsLinks->link_status = $model->status;
                            if(!$ProjectsLinks->save()){
                                $errors['projectLinks'][] = Yii::t('app', "$model->url was not added to project!").' ('.$ProjectsLinks->errors['link_id'][0].')';
                            }
                        }
                        else{
                            $errors['links'][] = Yii::t('app', "$model->url was not created!");
                        }
                    }
                }
            }
            if(!empty($errors['domains']) || !empty($errors['links']) || !empty($errors['projectLinks'])){
                $message = implode(' <br>', $errors['domains']).implode(' <br>', $errors['links']).implode(' <br>', $errors['projectLinks']);
                \Yii::$app->getSession()->setFlash('error',  $message);
                if(count($urls)>count($errors['domains'])&&count($urls)>count($errors['links'])&&count($urls)>count($errors['projectLinks'])){
                    \Yii::$app->getSession()->setFlash('success',  Yii::t('app', 'All rest valid links was added!'));
                }
            }
            else{
                \Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Links was successfully saved!'));
            }
            return $this->redirect(['projects-links/index', 'project_id' => $project_id]);
        }

        return $this->render('create', [
            'links' => $links,
            'project_id' => $project_id
        ]);
    }

    /**
     * Updates an existing ProjectsLinks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $linksForm = new LinksForm($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'linksForm' => $linksForm
        ]);
    }

    /**
     * Deletes an existing ProjectsLinks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $project_id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionChangeStatus(){
        $request = Yii::$app->request->post();
        if(Yii::$app->request->isAjax && @$request['status']>=0 && @$request['id']>0 && @$request['project_id']>0){
            $projectLink = ProjectsLinks::findOne(['id'=>$request['id'], 'project_id'=>$request['project_id']]);
            if($projectLink && !empty($statuses = $projectLink->getStatuses()) && isset($statuses[$request['status']])){
                $projectLink->link_status = $request['status'];
                if($projectLink->save()){
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Status was changed!'));
                    return $this->redirect(Yii::$app->getRequest()->getReferrer());
                }
                else{
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Status was not changed!'));
                    return $this->redirect(Yii::$app->getRequest()->getReferrer());
                }
            }
        }
        throw new NotFoundHttpException();
    }

    /**
     * Finds the ProjectsLinks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProjectsLinks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProjectsLinks::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
