<?php

namespace backend\controllers;

use common\models\LoginForm;
use Yii;
use common\models\Projects;
use common\models\ProjectsSearch;
use yii\helpers\BaseVarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\ProfileSearch;
use yii\web\UploadedFile;
use common\models\Links;
use common\models\LinksForm;
use common\components\parser\Parser;

use core\forms\ProjectsForm;
use core\services\ProjectService;
use core\entity\Project;
use yii\base\Module;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    private $service;

    public function __construct($id, Module $module, array $config = [], ProjectService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ]
        ];
    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetIframe($url=false, $layout=false){
        if($url){
            $url = base64_decode($url);
            $this->layout = $layout;
            return $this->renderPartial('iframe', [
                'url' => $url,
                'layout' => $layout
            ]);
        }
        return false;
    }

    public function actionGetUsers($layout=false)
    {
        $searchModel = new ProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination = ['pageSize' => 5];
        $dataProvider->sort = ['defaultOrder' => ['created_at' => SORT_DESC]];

        if($layout){
            $this->layout = $layout;
        }
        return $this->render('view-users', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Projects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $projectsForm = new ProjectsForm();
        if($projectsForm->load(Yii::$app->request->post())){
            $projectsForm->photo = $projectsForm->photo?$projectsForm->photo:UploadedFile::getInstance($projectsForm, 'photo');
            if($projectsForm->validate()){
                $project = $this->service->create($projectsForm);
                return $this->redirect(['view', 'id' => $project->id]);
            }
        }
        return $this->render('_create', [
            'projectsForm' => $projectsForm,
        ]);
    }

    public function actionUpdate($id){
        $project = $this->findModel($id);
        $projectsForm = new ProjectsForm($project);
        if($projectsForm->load(Yii::$app->request->post())){
            $projectsForm->photo = $projectsForm->photo?$projectsForm->photo:UploadedFile::getInstance($projectsForm, 'photo');
            if($projectsForm->validate()){
                $this->service->edit($projectsForm, $project->id);
                return $this->redirect(['view', 'id' => $project->id]);
            }
        }
        return $this->render('_update', [
            'project' => $project,
            'projectsForm' => $projectsForm
        ]);
    }

    /**
     * Deletes an existing Projects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Projects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
