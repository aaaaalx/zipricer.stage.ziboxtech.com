<?php

namespace backend\controllers;

use Yii;
use common\models\Profile;
use common\models\ProfileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\SignupForm;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * ProfileController implements the CRUD actions for Profile model.
 */
class ProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Profile models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Profile model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Profile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignupForm();
        $model->setScenario('userCreate');
        if ($model->load(Yii::$app->request->post())) {
            $model->icon = UploadedFile::getInstance($model, 'icon');

            if ($model->signup()) {
                Yii::$app->session->setFlash('success', "New user created!");
                return $this->redirect('/profile/index');
            }
            else{
                Yii::$app->session->setFlash('error', "User was not created!");
                return $this->redirect('/profile/index');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Profile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $profile = Profile::findIdentity($id);
        if (!$profile) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'User not exist!'));
            return $this->redirect('/profile/index');
        }
        $model = new SignupForm($profile);
        $model->password = '';
        if ($model->load(Yii::$app->request->post()) && $model->validate($model)) {
            $profile->edit($model->username, $model->email, $model->status, $model->referral_user_id, $model->referral_link);
            if($profile->user->edit($model->email, $model->password) && !$profile->user->save()){
                Yii::$app->session->setFlash('error', Yii::t('app', 'User not updated!'));
                return $this->redirect('/profile/index');
            }
            if ($profile->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'User updated!'));
                return $this->redirect('/profile/index');
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Profile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Profile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Profile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profile::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
