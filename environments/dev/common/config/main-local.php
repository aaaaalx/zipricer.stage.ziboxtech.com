<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=zipricer',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'coreDb' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=dev.med_app.ziboxtech.com;port=5433;dbname=zicore',
            'username' => 'zicore',
            'password' => 'dbpwd',
            'charset' => 'utf8',
        ],
//        'coreDb' => [
//            'class' => 'yii\db\Connection',
//            'dsn' => 'pgsql:host=localhost;port=5432;dbname=zicore',
//            'username' => 'postgres',
//            'password' => '',
//            'charset' => 'utf8',
//        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'parser' => [
            'class' => 'common\components\parser\Parser'
        ],
    ],
];
