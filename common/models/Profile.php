<?php

namespace common\models;

use core\entity\Project;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\BaseVarDumper;
use yii\web\IdentityInterface;
use yii\base\NotSupportedException;
use common\models\Users;
/**
 * This is the model class for table "profile".
 *
 * @property int $id
 * @property string $username
 * @property int $user_id
 * @property int $referral_user_id
 * @property string $referral_link
 * @property string $auth_key
 * @property string $email
 * @property int $status
 * @property string $icon
 * @property int $created_at
 * @property int $updated_at
 */
class Profile extends ActiveRecord implements IdentityInterface
{

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    public static function getStatuses(){
        $statuses = [];
        $statuses[self::STATUS_ACTIVE] = 'Active';
        $statuses[self::STATUS_DELETED] = 'Deleted';
        return $statuses;
    }

    public function edit($username, $email, $status=false, $referral_user_id=false, $referral_link=false, $user=false, $auth_key=false){
        $this->username = $username;
        $this->email = $email;
        $this->status = $status;
        $this->referral_user_id = $referral_user_id;
        $this->referral_link = $referral_link;
        $user?$this->user_id = $user:false;
        $auth_key?$this->auth_key = Yii::$app->security->generateRandomString(32):false;
        return $this;
    }

    public function generateReferralCode(){
        $code = Yii::$app->security->generateRandomString(10);
        if(!empty($this::find()->where(['referral_link' => $code])->all())){
            return $this->generateReferralCode();
        }
        return $code;
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%profile}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'user_id', 'auth_key', 'email'], 'required'],
            [['user_id', 'referral_user_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'referral_link', 'email', 'icon'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['user_id'], 'unique'],
            [['email'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'user_id' => Yii::t('app', 'User ID'),
            'referral_user_id' => Yii::t('app', 'Referral User ID'),
            'referral_link' => Yii::t('app', 'Referral Link'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'icon' => Yii::t('app', 'Icon'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function afterDelete()
    {
        parent::afterDelete();
        if(!empty($this->projects)){
            foreach ($this->projects as $project){
                $project->status = Project::DELETED;
                $project->save();
            }
        }
    }

    public function getUser()
    {
        return $this->hasOne(Users::class, ['user_id' => 'user_id']);
    }

    public function getProjects(){
        return $this->hasMany(Project::class, ['user_id' => 'id']);
    }

    public function getLinks(){
        $links = [];
        $linksCount = 0;
        $projectsCount = count($this->projects);
        if(!empty($this->projects)){
            foreach ($this->projects as $project){
                $linksCount += count($project->links);
                $links[$project->id] = $project->links;
            }
        }
        return ['links' => $links, 'linksCount' => $linksCount, 'projectsCount' => $projectsCount];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    public static function getUserByEmail($email){
        return Users::getByEmail($email);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->user->password);
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getUploadsPath($dir){
        return Yii::getAlias('@uploads').'\profiles\\'.$dir.'\\';
    }

    public function getUploadsUrl($dir, $fileName){
        return Yii::$app->params['frontendHost'].'uploads/profiles/'.$dir.'/'.$fileName;
    }

    public function getProjectsCount(){
        return Project::find()->where(['user_id'=>$this->id])->count();
    }

    public function getUserLinksCount(){
        $projects = $this->projects;
        if(is_array($projects)){
            return ProjectsLinks::find()->where(['in', 'project_id', array_column($projects, 'id')])->andWhere(['!=', 'link_status', ProjectsLinks::DELETED])->count();
        }
        return 0;
    }

    public function getActiveLinksCount(){
        $projects = $this->projects;
        if(is_array($projects)){
            return ProjectsLinks::find()->where(['in', 'project_id', array_column($projects, 'id')])->andWhere(['link_status'=>ProjectsLinks::ACTIVE])->count();
        }
        return 0;
    }

    public function getNotActiveLinksCount(){
        $projects = $this->projects;
        if(is_array($projects)){
            return ProjectsLinks::find()->where(['in', 'project_id', array_column($projects, 'id')])->andWhere(['link_status'=>ProjectsLinks::INACTIVE])->count();
        }
        return 0;
    }

    public function getHigherPriceLinksCount(){
        $statuses = [ProjectsLinks::INACTIVE,ProjectsLinks::DELETED];
        if(!empty($this->projects)){
            $projectsIds = array_column($this->projects, 'id');
            //$projectsPrices = array_column($this->projects, 'price');
            //$linksHistoryQuery = LinksHistory::find()->joinWith(['link.links.project'=>function($query)use($statuses, $projectsIds, $projectsPrices){
            //return $query->where(['in','projects_links.project_id', $projectsIds])->andWhere(['not in', 'link_status', $statuses])->andWhere(['>','links_history.price', 'projects.price']);
            //}])->groupBy(['link_id']);
            //$linksHistoryQuery->all();
            $res = Yii::$app->db->createCommand('SELECT projects_links.* FROM projects_links LEFT JOIN links_history ON links_history.link_id = projects_links.link_id LEFT JOIN projects ON projects.id = projects_links.project_id WHERE (project_id IN ('.implode(',', $projectsIds).')) AND (link_status NOT IN ('.implode(',', $statuses).')) AND (links_history.price > projects.price) GROUP BY link_id ORDER BY created_at DESC');
            return $res->execute();
        }
        return 0;
    }

    public function getLowerPriceLinksCount(){
        $statuses = [ProjectsLinks::INACTIVE,ProjectsLinks::DELETED];
        if(!empty($this->projects)){
            $projectsIds = array_column($this->projects, 'id');
            $res = Yii::$app->db->createCommand('SELECT projects_links.* FROM projects_links LEFT JOIN links_history ON links_history.link_id = projects_links.link_id LEFT JOIN projects ON projects.id = projects_links.project_id WHERE (project_id IN ('.implode(',', $projectsIds).')) AND (link_status NOT IN ('.implode(',', $statuses).')) AND (links_history.price < projects.price) GROUP BY link_id ORDER BY created_at DESC');
            return $res->execute();
        }
        return 0;
    }

    public function getSamePriceLinksCount(){
        $statuses = [ProjectsLinks::INACTIVE,ProjectsLinks::DELETED];
        if(!empty($this->projects)){
            $projectsIds = array_column($this->projects, 'id');
            $res = Yii::$app->db->createCommand('SELECT projects_links.* FROM projects_links LEFT JOIN links_history ON links_history.link_id = projects_links.link_id LEFT JOIN projects ON projects.id = projects_links.project_id WHERE (project_id IN ('.implode(',', $projectsIds).')) AND (link_status NOT IN ('.implode(',', $statuses).')) AND (links_history.price = projects.price) GROUP BY link_id ORDER BY created_at DESC');
            return $res->execute();
        }
        return 0;
    }

    public function getNotAvailablePriceLinksCount(){
        $statuses = [ProjectsLinks::INACTIVE,ProjectsLinks::DELETED];
        if(!empty($this->projects)){
            $projectsIds = array_column($this->projects, 'id');
            $projectsLinksQuery = ProjectsLinks::find()->where(['in', 'project_id', $projectsIds])->andWhere(['not in', 'link_status', $statuses])->joinWith('history')->andWhere(['is', 'links_history.link_id', null]);
            return $projectsLinksQuery->count();
        }
        return 0;
    }

    public function getRandLinks($limit=0){
        $statuses = [ProjectsLinks::INACTIVE,ProjectsLinks::DELETED];
        if(!empty($this->projects)) {
            $projectsIds = array_column($this->projects, 'id');
            $randLinks = ProjectsLinks::find()
                ->where(['in', 'projects_links.project_id',$projectsIds])
                ->joinWith('history', true, 'INNER JOIN')
                ->andWhere(['is not', 'links_history.price', null])
                ->andWhere(['!=', 'links_history.status', LinksHistory::DELETED])
                ->andWhere(['>', 'links_history.price', 0])
                ->andWhere(['not in', 'projects_links.link_status', $statuses])
                ->groupBy('projects_links.link_id')
                ->orderBy(new Expression('rand()'))
                ->limit($limit)
                ->all();
            return is_array($randLinks)?$randLinks:[];
        }
        return [];
    }

}
