<?php

namespace common\models;

use core\entity\Project;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "projects_links".
 *
 * @property int $id
 * @property int $link_id
 * @property int $project_id
 * @property string $options
 * @property int $link_status
 * @property int $created_at
 * @property int $updated_at
 */
class ProjectsLinks extends ActiveRecord
{
    const ACTIVE = 1;
    const INACTIVE = 2;
    const DELETED = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects_links';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link_id', 'project_id', 'link_status'], 'required'],
            [['link_id', 'project_id', 'link_status', 'created_at', 'updated_at'], 'integer'],
            [['options'], 'string', 'max' => 1000],
            ['link_id', 'unique', 'filter' => $this->link_id&&$this->project_id?['and', ['<>', 'link_id', $this->link_id], ['<>', 'project_id', $this->project_id]]:null]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'link_id' => Yii::t('app', 'Link ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'options' => Yii::t('app', 'Options'),
            'link_status' => Yii::t('app', 'Link Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public static function getStatuses($status=null){
        $statuses = [
            self::ACTIVE=>Yii::t('app', 'Active'),
            self::INACTIVE=>Yii::t('app', 'Inactive')
        ];
        if(isset($statuses[$status])){
            return $statuses[$status];
        }
        return $statuses;
    }

    public function getLink(){
        return $this->hasOne(Links::class, ['id' => 'link_id']);
    }

    public function getHistory(){
        return $this->hasMany(LinksHistory::class, ['link_id' => 'link_id']);
    }

    public function getProject(){
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

}
