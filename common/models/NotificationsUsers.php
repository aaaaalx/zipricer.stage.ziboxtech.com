<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notifications_users".
 *
 * @property int $id
 * @property int $notification_id
 * @property int $profile_id
 * @property int $status
 */
class NotificationsUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notification_id', 'profile_id'], 'required'],
            [['notification_id', 'profile_id', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'notification_id' => Yii::t('app', 'Notification ID'),
            'profile_id' => Yii::t('app', 'Profile ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
