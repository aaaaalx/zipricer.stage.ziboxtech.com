<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use common\helpers\MainHelper;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property string $name
 * @property string $photo
 * @property string $description
 * @property string $vendor_code
 * @property string $price
 * @property int $user_id
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Projects extends ActiveRecord
{
    const ACTIVE = 1;
    const IN_CONSIDERED = 2;
    const INACTIVE = 3;
    const DELETED = 0;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ]
        ];
    }

    public static function getStatuses(){
        return [
            self::ACTIVE => 'Active',
            self::IN_CONSIDERED => 'In considered',
            self::INACTIVE => 'Inactive',
            self::DELETED => 'Deleted'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['photo', 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'minWidth' => 100, 'maxWidth' => 1000, 'minHeight' => 100, 'maxHeight' => 1000,],
            [['name', 'user_id', 'status'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id', 'price', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'vendor_code'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
            [['name'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
            'photo' => Yii::t('app','Photo'),
            'description' => Yii::t('app','Description'),
            'vendor_code' => Yii::t('app','Vendor Code'),
            'price' => Yii::t('app','Price'),
            'user_id' => Yii::t('app','User ID'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }

    public function afterDelete()
    {
        parent::afterDelete();

    }

    public function uploadFiles(UploadedFile $fileInstanse){
        $file = MainHelper::setUniqueFileName($fileInstanse->baseName, $fileInstanse->extension);
        $path = $this->getUploadsPath();
        if(!is_dir($path)){
            mkdir($path, 0777, true);
        }
        if($fileInstanse->saveAs($path.$file)){
            return $file;
        }
        return false;
    }

    public function deleteFiles($name=false){
        $path = $this->getUploadsPath();
        if(is_dir($path)){
            if(file_exists($path.$name)){
                unlink($path.$name);
                MainHelper::remove_if_dir_empty($path);
            }
            else{
                MainHelper::remove_dir($path);
            }
        }
    }

    public function getUploadsUrl(){
        return Yii::$app->params['frontendHost'].'uploads/projects/'.$this->id.'/';
    }

    public function getUploadsPath(){
        return Yii::getAlias('@uploads').'\projects\\'.$this->id.'\\';
    }

    public function getUser(){
        return $this->hasOne(Profile::class, ['id' => 'user_id']);
    }

}
