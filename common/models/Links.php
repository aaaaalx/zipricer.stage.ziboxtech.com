<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\BaseVarDumper;

/**
 * This is the model class for table "links".
 *
 * @property int $id
 * @property string $link_url
 * @property integer $status
 * @property int $created_at
 * @property int $updated_at
 */
class Links extends ActiveRecord
{
    const AVAILABLE = 1;
    const UNAVAILABLE = 0;

    protected $lastUpdate = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'links';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link_url'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['link_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'link_url' => Yii::t('app', 'Link Url'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public static function getStatuses($status=null){
        $statuses = [
            self::AVAILABLE=>Yii::t('app', 'Available'),
            self::UNAVAILABLE=>Yii::t('app', 'Unavailable')
        ];
        if(isset($statuses[$status])){
            return $statuses[$status];
        }
        return $statuses;
    }

    public function getHistory(){
        return $this->hasMany(LinksHistory::class, ['link_id' => 'id'])->orderBy(['created_at' => SORT_DESC]);
    }

    public function getLinks(){
        return $this->hasMany(ProjectsLinks::class, ['link_id' => 'id'])->orderBy(['created_at' => SORT_DESC]);
    }

    public function getLastUpdate(){
        return $this->lastUpdate?$this->lastUpdate:$this->lastUpdate = LinksHistory::find()->where(['link_id'=>$this->id])->orderBy(['id' => SORT_DESC])->one();
    }

}
