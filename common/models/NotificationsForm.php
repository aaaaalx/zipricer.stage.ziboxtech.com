<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "notifications".
 *
 * @property string $title
 * @property string $message
 * @property int $status
 * @property string $publication_at
 */
class NotificationsForm extends Model
{
    public $title;
    public $message;
    public $status;
    public $publication_at;

    public function __construct(Notifications $notifications = null, array $config = [])
    {
        parent::__construct($config);
        $this->title = $notifications->title;
        $this->message = $notifications->message;
        $this->status = $notifications->status;
        $this->publication_at = $notifications->publication_at;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'message', 'status'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
            ['publication_at', 'string', 'max' => 50],
            [['message'], 'string', 'max' => 2000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'publication_at' => Yii::t('app', 'Publication'),
        ];
    }

}