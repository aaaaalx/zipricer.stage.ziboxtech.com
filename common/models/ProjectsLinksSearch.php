<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use kartik\daterange\DateRangeBehavior;

/**
 * ProjectsLinksSearch represents the model behind the search form of `common\models\ProjectsLinks`.
 */
class ProjectsLinksSearch extends ProjectsLinks
{

    public $link;
    public $availability;
    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;
    public $updateTimeRange;
    public $updateTimeStart;
    public $updateTimeEnd;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::class,
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ],
            [
                'class' => DateRangeBehavior::class,
                'attribute' => 'updateTimeRange',
                'dateStartAttribute' => 'updateTimeStart',
                'dateEndAttribute' => 'updateTimeEnd',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'link_id', 'project_id', 'link_status', 'availability'], 'integer'],
            [['link'], 'safe'],
            [['createTimeRange', 'updateTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $project_id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $project_id)
    {
        $query = ProjectsLinks::find()->where('project_id = :project_id',[':project_id'=>$project_id])->joinWith('link');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);

        $dataProvider->sort->attributes['link'] = [
            'asc' => ['links.link_url' => SORT_ASC],
            'desc' => ['links.link_url' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['availability'] = [
            'asc' => ['links.status' => SORT_ASC],
            'desc' => ['links.status' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            //$query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'projects_links.id' => trim($this->id),
            'projects_links.link_id' => trim($this->link_id),
            'projects_links.project_id' => trim($this->project_id),
            'projects_links.link_status' => $this->link_status,
            'links.status' => $this->availability
        ]);

        if($this->createTimeStart && $this->createTimeEnd){
            $query->andFilterWhere(['>=', 'projects_links.created_at', $this->createTimeStart])->andFilterWhere(['<', 'projects_links.created_at', $this->createTimeEnd]);
        }
        if($this->updateTimeStart && $this->updateTimeEnd){
            $query->andFilterWhere(['>=', 'projects_links.updated_at', $this->updateTimeStart])->andFilterWhere(['<=', 'projects_links.updated_at', $this->updateTimeEnd]);
        }
        $query->andFilterWhere(['like', 'links.link_url', trim($this->link)]);

        return $dataProvider;
    }
}
