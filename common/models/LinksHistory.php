<?php

namespace common\models;

use core\entity\Project;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "links_history".
 *
 * @property int $id
 * @property string $link_id
 * @property string $link_name
 * @property string $domain
 * @property int $last_parse
 * @property string $errors
 * @property int $start_time
 * @property int $end_time
 * @property int $price
 * @property int $discount
 * @property int $availability
 * @property int $last_change
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class LinksHistory extends ActiveRecord
{
    const ACTIVE = 1;
    const DELETED = 0;

    public $availabilities = [
        'available' => 1,
        'unavailable' => 0
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'links_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link_id', 'link_name', 'domain', 'status', 'created_at', 'updated_at'], 'required'],
            [['last_parse', 'start_time', 'end_time', 'price', 'discount', 'availability', 'last_change', 'status', 'created_at', 'updated_at'], 'integer'],
            [['link_id', 'link_name'], 'string', 'max' => 255],
            [['domain'], 'string', 'max' => 60],
            [['errors'], 'string', 'max' => 2000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'link_id' => Yii::t('app', 'Link ID'),
            'link_name' => Yii::t('app', 'Link Name'),
            'domain' => Yii::t('app', 'Domain'),
            'last_parse' => Yii::t('app', 'Last Parse'),
            'errors' => Yii::t('app', 'Errors'),
            'start_time' => Yii::t('app', 'Start Time'),
            'end_time' => Yii::t('app', 'End Time'),
            'price' => Yii::t('app', 'Price'),
            'discount' => Yii::t('app', 'Discount'),
            'availability' => Yii::t('app', 'Availability'),
            'last_change' => Yii::t('app', 'Last Change'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getLink(){
        return $this->hasOne(Links::class, ['id' => 'link_id']);
    }


    public function getProjectsLinks(){
        return $this->hasMany(ProjectsLinks::class, ['link_id' => 'link_id']);
    }

    /*
    public function getProject(){
        return $this->hasOne(Project::class, ['id' => 'project_id', 'profile_id'=>Yii::$app->user->id])->viaTable('projects_links', ['link_id'=>'link_id']);
    }
    */
}
