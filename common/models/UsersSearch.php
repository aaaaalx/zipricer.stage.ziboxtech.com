<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class UsersSearch extends Users
{
    public function rules()
    {
        // только поля определенные в rules() будут доступны для поиска
        return [
            [['user_id'], 'integer'],
            [['username'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {

        $query = Users::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'user_id' => SORT_DESC,
                ]
            ],
        ]);

        // загружаем данные формы поиска и производим валидацию
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // изменяем запрос добавляя в его фильтрацию
        $query->andFilterWhere(['user_id' => trim($this->user_id)]);
        $query->andFilterWhere(['like', 'username', trim($this->username)]);
        //$query->andFilterWhere(['like', 'email', trim($this->email)]);
        //$query->andFilterWhere(['like', 'created_at', $this->created_at]);

        return $dataProvider;
    }
}