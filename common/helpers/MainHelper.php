<?php
namespace common\helpers;


class MainHelper
{

    public static function randHash($value='', $len=32, $addHashToValue=false){
        $timestamp = strtotime(date('Y-m-d H:i:s'));
        if($value && $addHashToValue){
            $value = $value.'_'.substr(md5($value.$timestamp),-$len);
        }
        elseif($value){
            $value = substr(md5($value.$timestamp),-$len);
        }
        else{
            $value = substr(md5(rand().$timestamp),-$len);
        }
        return $value;
    }

    public static function setUniqueFileName($name, $extension, $addHash=true, $length=12){
        $name = str_replace(' ', '_',$name);
        return self::randHash($name,$length,$addHash).'.'.$extension;
    }

    public static function is_dir_empty($path) {
        if (!is_readable($path)) return NULL;
        return (count(scandir($path)) == 2);
    }

    public static function remove_if_dir_empty($path) {
        if(self::is_dir_empty($path)){
            rmdir($path);
        }
    }

    //remove dir with all files
    public static function remove_dir($path){
        array_map('unlink', glob($path."*.*"));
        rmdir($path);
    }

    //remove all files in dir
    public static function removefiles($path){
        array_map('unlink', glob($path."*.*"));
    }

    public static function readCsv($file){
        $res = [];
        if(($h = fopen($file, "r")) !== false){
            while(($data = fgetcsv($h)) !== false){
                if(isset($data[0]) && trim($data[0])){
                    $dataA = explode(';',$data[0]);
                    if(count($dataA)>0){
                        foreach($dataA as $s){
                            $s = trim($s);
                            $s?$res[] = $s:false;
                        }
                    }
                    else if(!is_array($dataA)){
                        $res[] = $dataA;
                    }
                }
            }
            fclose($h);
        }
        if(!count($res)>0){
            return false;
        }
        return $res;
    }

    public static function valBetween($val, $min, $max) {
        return (min($max, max($min, $val)));
    }

    //cut string by passed letters amount. can be added ending for cut strings
    public static function cutString($string, $count=20, $end='...'){
        $result = '';
        if(is_string($string) && mb_strlen(trim($string))>0){
            $contentCount = mb_strlen($string);
            $result = mb_substr($string, 0, $count);
            if($contentCount>mb_strlen($result)){
                $result .= $end;
            }
        }
        return $result;
    }

}
