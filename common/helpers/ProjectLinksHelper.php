<?php

namespace common\helpers;

class ProjectLinksHelper
{
    public static function chartConfig($item){
        return ['year' => date('d.m.Y h:i:s', $item->created_at), 'price' => $item->price];
    }

}