<?php

namespace common\components\parser\classes;

use yii\db\Query;
use common\models\Links;
use yii\helpers\BaseVarDumper;

class ParserRepository implements ParserRepositoryInterface
{

    public static $errors = [];

    public static function getLinks(){
        $links = (new Query())->select(['id','link_url'])->from('links')->where(['status' => Links::AVAILABLE])->orderBy('id');
        return $links?$links:false;
    }

    public static function saveLinksHistory(array $data){
        $values = [];
        foreach ($data as $url => $value){
            $columns = [];
            if(empty($value['link_id']) || empty($value['domain']) || empty($value['link_name']) || @$value['status']<0){
                self::$errors[$url]['text'] = 'No set required values!';
                continue;
            }
            $columns['link_id'] = $value['link_id'];
            $columns['link_name'] = $value['link_name'];
            $columns['domain'] = $value['domain'];
            $columns['errors'] = !empty($data[$url]['errors'])?json_encode($value['errors'], true):null;
            $columns['start_time'] = isset($value['start_time'])&&is_numeric($value['start_time'])?(int)$value['start_time']:0;
            $columns['end_time'] = isset($value['end_time'])&&is_numeric($value['end_time'])?(int)$value['end_time']:0;
            $columns['price'] = isset($value['price'])&&is_numeric($value['price'])?(int)$value['price']:0;
            $columns['discount'] = isset($value['discount'])&&is_numeric($value['discount'])?(int)$value['discount']:0;
            $columns['availability'] = isset($value['availability'])&&is_numeric($value['availability'])?(int)$value['availability']:0;
            $columns['status'] = (int)$value['status'];
            $columns['created_at'] = strtotime('now');
            $values[] = $columns;
        }
        $columns = ['link_id','link_name', 'domain', 'errors','start_time', 'end_time', 'price', 'discount', 'availability', 'status', 'created_at'];
        if(!empty($values)){
            try {
                return \Yii::$app->db
                    ->createCommand()
                    ->batchInsert('links_history', $columns, $values)
                    ->execute();
            } catch (\Exception $e) {
                \Yii::$app->db->close();
                \Yii::$app->db->open();
                return self::saveLinksHistory($data);
            }
        }
        return false;
    }

    public static function getLastChangedData($link_id){
        //return \Yii::$app->db->createCommand('SELECT id FROM links_history')->queryColumn();
        //return \Yii::$app->db->createCommand('SELECT a.* FROM links_history AS a WHERE a.price <> ( SELECT b.price FROM links_history AS b WHERE a.link_id = b.link_id AND a.created_at > b.created_at ORDER BY b.created_at DESC LIMIT 1) ORDER BY created_at DESC LIMIT 1')->queryColumn();
        //return \Yii::$app->db->createCommand('SELECT a.* FROM links_history AS a WHERE a.price <> ( SELECT b.price FROM links_history AS b WHERE a.link_id = b.link_id AND a.created_at > b.created_at ORDER BY b.created_at DESC LIMIT 1) OR a.discount <> ( SELECT b.discount FROM links_history AS b WHERE a.link_id = b.link_id AND a.created_at > b.created_at ORDER BY b.created_at DESC LIMIT 1) ORDER BY created_at DESC LIMIT 1')->queryColumn();
        return \Yii::$app->db->createCommand('SELECT id, created_at, price, discount, link_id FROM ( SELECT (@pricePre <> price AND @linkPre=link_id) AS priceChanged, id, link_id, created_at, price, discount, @pricePre := price, @linkPre := link_id FROM links_history, (SELECT @pricePre:=NULL, @linkPre:=NULL) AS d ORDER BY created_at ) AS good WHERE priceChanged ORDER BY created_at DESC')->queryAll();
    }

}