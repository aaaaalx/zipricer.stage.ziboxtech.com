<?php

namespace common\components\parser\classes;

abstract class WebsiteParser
{
    const AVAILABLE = 1;
    const UNAVAILABLE = 0;

    public $errors = [];
    public $connection;
    public $fields = [
        'link_name' => null,
        'domain' => null,
        'last_parse' => null,
        'errors' => null,
        'start_time' => null,
        'end_time' => null,
        'price' => null,
        'discount' => null,
        'availability' => null,
        'status' => null
    ];

    public $name;
    protected $resultFields = [];
    protected $config = [];
    protected $commonConfig = [];
    protected $availability = [
        'available' => 1,
        'unavailable' => 0
    ];

    public function __construct(array $config){
        $this->setConfig();
        $this->commonConfig = $config;
        $this->connection = new Connection($config);
    }

    public function getConfig(){
        return $this->config;
    }

    public function setConfig(array $config = []){
        return $this->config = !$config?$this->config:array_merge($config, $this->config);
    }

    abstract public function getData(array $urls);
}