<?php

namespace common\components\parser\classes;


interface ParserRepositoryInterface
{
    public static function getLinks();
    public static function saveLinksHistory(array $data);
}