<?php

namespace common\components\parser\classes;

use Symfony\Component\DomCrawler\Crawler;
use yii\helpers\BaseVarDumper;

class Parser_comfy extends WebsiteParser
{

    public $name = 'comfy';
    protected $config = [
        'recall_on_404' => false
    ];

    public function getData(array $urls){
        return;
        if(empty($urls)){
            return ['domain' => BaseParser::getDomains()[$this->name], 'error' => 'Empty urls list!'];
        }
        $content = [];
        $proxies = [];
        if($this->config['use_proxy']){//if proxies active
            $proxies = $this->config['proxies'];
        }
        foreach ($urls as $id => $url){
            $errors = [];
            if($this->commonConfig['time_between_requests']){
                sleep((int)$this->commonConfig['time_between_requests']);
            }
            $time_start = strtotime('now');
            $result = $this->connection->getConnection($url, $proxies, $this->commonConfig['retries_number'], $this->commonConfig['use_headers'], $this->commonConfig['check_if_url_exists'], $this->config['recall_on_404']);
            $time_end = strtotime('now');
            if(!empty($result['content'])){
                $crawler = new Crawler($result['content']);
                $title = $crawler->filter('h1');
                $title = $title->count()?trim($title->text()):'Undefined';
                $price = $crawler->filter('#productCard .product-card__actions .price-value');
                $price = $price->count()&&$price->attr('content')?$price->attr('content'):0;
                $discount = $crawler->filter('#productCard .product-card__actions .price-box__content_special .price-value');
                $discount = $discount->count()&&$discount->text()?preg_replace('/[^0-9]/', '', $discount->text()):0;
                $availability = $crawler->filter('#productCard .product-card__btns .gtm-button-buy');
                $availability = $availability->count()?1:0;
                $status = $price?1:0;
                $errors['links'] = isset($result['errors'][$url])?$result['errors'][$url]:[];
                $errors['values'] = isset($this->errors[$url])?$this->errors[$url]:[];
                $content[$url] = [
                    'link_name' => trim(htmlspecialchars($title, ENT_QUOTES)),
                    'price' => trim(htmlspecialchars($price, ENT_QUOTES)),
                    'discount' => trim(htmlspecialchars($discount, ENT_QUOTES)),
                    'availability' => $availability,
                    'domain' => BaseParser::getDomains()[$this->name],
                    'status' => $status,
                    'start_time' => $time_start,
                    'end_time' => $time_end,
                    'errors' => $errors,
                    'link_id' => $id
                ];
            }
            else{
                $this->errors[$url]['text'] = 'Empty content!';
            }
            $this->connection->errors?$this->errors[$url]['connection'] = $this->connection->errors:false;
        }
        return ['values' => $content, 'errors' => $this->errors];
    }

}