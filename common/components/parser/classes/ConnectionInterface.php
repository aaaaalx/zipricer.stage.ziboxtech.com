<?php

namespace common\components\parser\classes;

interface ConnectionInterface
{
    public function __construct(array $config);
    public function getHeaders();
    public function getConnection();
    public function is_url_exist($url);
}