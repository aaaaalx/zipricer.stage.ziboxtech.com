<?php


namespace common\components\parser\classes;

use yii\helpers\BaseVarDumper;

class BaseParser{

    public $cronConfig = [];
    protected $config = [];
    protected $proxies = [
        [
            'url' => '',
            'port' => ''
        ]
    ];
    protected $rules = [
        'rozetka'=>"/[^\.\/]+\.[^\.\/]+\.[^\.\/]+$/",
        'comfy'=>"/[^\.\/]+\.[^\.\/]+$/",
        'allo'=>"/[^\.\/]+\.[^\.\/]+$/",
    ];

    public function __construct(){
        $this->setConfig();
    }

    public function getConfig(){
        return $this->config;
    }

    public function setConfig(array $config = []){
        $defaultConfig = [
            'proxies' => $this->proxies,
            'time_between_requests' => 5,//in seconds. 0 - without delay
            'response_timeout' => 120,//in seconds
            'retries_number' => 5,
            'retries_number_proxy' => 5,
            'max_proxies_used' => 5,//-1 - all
            'max_parsing_duration' => 720,//in minutes
            'use_proxy' => false,
            //'use_threads' => false,
            //'max_threads_count' => 5,
            'use_curl' => true,
            'check_if_url_exists' => false,
            'use_headers' => true,
            'allow_redirects' => true
        ];
        return $this->config = !$config?$defaultConfig:array_merge($config, $defaultConfig);
    }

    public static function getDomains(){
        return [
            'rozetka'=>'rozetka.com.ua',
            'comfy'=>'comfy.ua',
            'allo'=>'allo.ua',
        ];
    }

    public function run(){
        $parser = new Parsers($this->config, self::getDomains(), $this->config);
        $parser->start();
    }

    public function runByUrl($url, $save){
        $parser = new Parsers($this->config, self::getDomains(), $this->config);
        $parser->startByUrl($url, $save);
    }

}