<?php


namespace common\components\parser\classes;

use yii\helpers\BaseVarDumper;

class Parser_rozetka extends WebsiteParser
{
    public $name = 'rozetka';
    protected $mainRequestUrl = 'https://rozetka.com.ua/recent_recommends/action=getGoodsDetailsJSON/?goods_ids=';
    protected $config = [
        'max_count_ids_at_once' => 2,
        'recall_on_404' => false
    ];

    protected $resultFields = [
        'title' => 'link_name',
        'old_price' => 'price',
        'price' => 'discount',
        'sell_status' => 'availability'
    ];

    public function getData(array $urls){
        return;
        //get ids list
        $ids = $this->getPagesIDs($urls);
        $urls = @$ids['urls'];
        $ids = @$ids['ids'];
        if(isset($ids['error'])){
            return $ids['error'];
        }
        //queue list
        if(!empty($ids)){
            $request = [];
            $queue = 0;
            $currentCount = 1;
            $count = $this->config['max_count_ids_at_once'];
            foreach ($ids as $id => $url){
                $request[$queue] = empty($request[$queue])?$id:$request[$queue].','.$id;
                if($currentCount == $count || $currentCount == count($ids)){
                    $request[$queue] = $this->mainRequestUrl.$request[$queue];
                    ++$queue;
                    $count += $currentCount;
                }
                ++$currentCount;
            }
            //get contents
            $content = [];
            $proxies = [];
            if($this->config['use_proxy']){//if proxies active
                $proxies = $this->config['proxies'];
            }
            foreach ($request as $query){
                //time between requests
                if($this->commonConfig['time_between_requests']){
                    sleep((int)$this->commonConfig['time_between_requests']);
                }
                //connect to url
                $time_start = strtotime('now');
                $result = $this->connection->getConnection($query, $proxies, $this->commonConfig['retries_number'], $this->commonConfig['use_headers'], $this->commonConfig['check_if_url_exists'], $this->config['recall_on_404']);
                $time_end = strtotime('now');
                if(!empty($result['content']) && empty($result['error'])){
                    $resultArr = json_decode($result['content'], true);
                    if(!empty($resultArr['content'])){
                        $resultArr['content'] = array_map(function($v)use($query, $time_start, $time_end){
                            if(is_array($v)){
                                $v['url'] = $query;
                                $v['time_start'] = $time_start;
                                $v['time_end'] = $time_end;
                            }
                            return $v;
                        }, $resultArr['content']);
                        $content = array_merge($content, $resultArr['content']);
                    }
                    else{
                        $this->errors[$query]['getData']['text'] = 'Wrong json!';
                    }
                }
                elseif(empty($resultArr['content'])){
                    $this->errors[$query]['getData']['text'] = 'No content!';
                }
                $content['errors'][$query]['connection'] = $this->connection->errors;
                $content['errors'][$query]['data'] = $this->errors[$query];
            }
            $content_ids = [];
            foreach ($content as $key => $item){
                if(isset($item['content']['id'])){
                    $content_ids[$item['content']['id']] = $item;
                }
                else{
                    if(!isset($content_ids[$key])){
                        $content_ids[$key] = $item;
                    }
                    else{
                        $content_ids[count($content)] = $item;
                    }
                }
            }
            $values = $this->getValues($content_ids);
            $unitedResult = $this->uniteResult($values, $ids, $urls);
            if(!empty($unitedResult['empty'])){
                $this->errors['empty_ids'] = $unitedResult['empty'];
            }
            return ['values' => $unitedResult['result'], 'errors' => $this->errors];
        }
        return ['domain' => BaseParser::getDomains()[$this->name], 'error' => 'Empty ids list!'];
    }

    protected function uniteResult(array $values, array $ids, array $urls){
        $result = [];
        $empty = [];
        foreach ($ids as $id => $url){
            if(isset($values[$id])){
                $values[$id]['link_id'] = array_search($url, $urls);
                $result[$url] = $values[$id];
            }
            else{
                $result[$url] = ['link_id'=>array_search($url, $urls), 'link_name'=>'Undefined', 'domain'=>BaseParser::getDomains()[$this->name], 'status'=>self::UNAVAILABLE];
                $empty[] = $id;
            }
        }
        return ['result' => $result, 'empty' => $empty];
    }

    protected function getValues(array $content){
        $fields = [];
        $errors = [];
        if(!empty($content['errors'])){
            $errors = $content['errors'];
            unset($content['errors']);
        }
        foreach ($content as $key => $item){
            $fields[$key] = $this->fields;
            $url = !empty($item['url'])?$item['url']:null;
            if(!$url){
                $this->errors[$url]['getValues']['text'] = 'Empty url!';
            }
            elseif(!empty($item['content'])){
                if(!empty($item['content']['title'])){
                    $fields[$key][$this->resultFields['title']] = trim(htmlspecialchars($item['content']['title'], ENT_QUOTES));
                }
                if(!empty($item['content']['old_price'])){
                    $fields[$key][$this->resultFields['old_price']] = trim(htmlspecialchars($item['content']['old_price'], ENT_QUOTES));
                }
                if(!empty($item['content']['price']) && empty($item['content']['old_price'])){
                    $fields[$key][$this->resultFields['old_price']] = trim(htmlspecialchars($item['content']['price'], ENT_QUOTES));
                }
                elseif(!empty($item['content']['price'])){
                    $fields[$key][$this->resultFields['price']] = trim(htmlspecialchars($item['content']['price'], ENT_QUOTES));
                }
                if(!empty($item['content']['sell_status'])){
                    $sell_status = (int)@$this->availability[strtolower($item['content']['sell_status'])];
                    $fields[$key][$this->resultFields['sell_status']] = $sell_status>=0?$sell_status:0;
                }
                $fields[$key]['domain'] = BaseParser::getDomains()[$this->name];
                $fields[$key]['status'] = !empty($fields[$key]['price'])?self::AVAILABLE:self::UNAVAILABLE;
            }
            else{
                $this->errors[$key]['getValues']['text'] = 'Empty response!';
            }
            $fields[$key]['start_time'] = $item['time_start'];
            $fields[$key]['end_time'] = $item['time_end'];
            $fields[$key]['errors']['links'] = isset($errors['errors'][$url])?$errors['errors'][$url]:[];
            $fields[$key]['errors']['values'] = isset($this->errors[$key])?$this->errors[$key]:[];
        }
        return $fields;
    }

    protected function getPagesIDs(array $urls){
        if(empty($urls)){
            return ['domain' => BaseParser::getDomains()[$this->name], 'error' => 'Empty urls list!'];
        }
        $ids = [];
        foreach ($urls as $key => $url){
            $url = preg_replace('{/$}', '', $url);
            $urls[$key] = $url;
            $urlParse = parse_url($url);
            $id = null;
            if(isset($urlParse['path'])){
                $id = substr($urlParse['path'], strrpos($urlParse['path'], '/')+2);
                $id?$ids[$id] = $url:$this->errors[$url]['text'] = 'Can\'t get id from url!';
            }
        }
        return ['ids' => $ids, 'urls' => $urls];
    }

}