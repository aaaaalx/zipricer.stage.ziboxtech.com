<?php

namespace common\components\parser\classes;

use yii\helpers\BaseVarDumper;

class Parsers{

    public $params;
    public $errors = [];
    public $config = [];
    public $domains = [];
    public $rules = [];
    public $classPath = 'common\components\parser\classes\\';
    protected $parsers = [];
    protected $batchSize = 100;

    public function __construct(array $config, array $domains, array $rules){
        $this->config = $config;
        $this->domains = $domains;
        $this->rules = $rules;
    }

    protected function getParser($domain){
        $class = 'Parser_'.$domain;
        $classPath = $this->classPath.$class;
        if(!isset($this->parsers[$domain]) && file_exists(__DIR__.'\\'.$class.'.php')){
            return $this->parsers[$domain] = class_exists($classPath)?new $classPath($this->config):null;
        }
        return false;
    }

    public function startByUrl($url, $save = false){
        $links = is_array($url)?$url:[$url];
        $results = [];
        if($links){
            $result = [];
            foreach ($links->batch($this->batchSize) as $linksPack){
                $result = $this->getContent(array_column($linksPack, 'link_url', 'id'));
                if(!empty($result)){
                    foreach ($result as $domain => $data){
                        if($save && !empty($data['values'])){
                            $results[] = ParserRepository::saveLinksHistory($data['values']);
                        }
                    }
                }
            }
            return $result;
        }
        $this->errors['start']['text'] = 'No links found!';
        return ['error' => $this->errors['start']['text']];
    }

    public function start(){
        $links = ParserRepository::getLinks();
        $results = [];
        if($links){
            //$result = [];
            //add batch count
            foreach ($links->batch($this->batchSize) as $linksPack){
                $result = $this->getContent(array_column($linksPack, 'link_url', 'id'));
                if(!empty($result)){
                    foreach ($result as $domain => $data){
                        if(!empty($data['values'])){
                            $results[] = ParserRepository::saveLinksHistory($data['values']);
                        }
                    }
                }
            }
            //return $result;
        }

        echo '<br>ParserRepository = ';
        print_r($results);

        die;
        $this->errors['start']['text'] = 'No links found!';
        return ['error' => $this->errors['start']['text']];
    }

    public function getContent(array $urls){
        $links = $this->prepareLinks($urls, $this->domains);
        $result = [];
        if(!empty($links)){
            foreach ($links as $domain => $items){
                if(!isset($this->parsers[$domain])){
                    $domainParser = $this->getParser($domain);
                }
                else{
                    $domainParser = $this->parsers[$domain];
                }
                if($domainParser){
                    if(!isset($result[$domain])){
                        $result[$domain] = [];
                    }
                    $data = $domainParser->getData($items);
                    if(!empty($data)){
                        $result[$domain] = array_merge($result[$domain], $data);
                    }
                }
                else{
                    $this->errors[$domain]['getSingleContent']['text'] = 'No domain parser found!';
                }
            }
        }
        return $result;
    }

    public function prepareLinks(array $urls, array $domains){
        $links = [];
        foreach ($urls as $id => $url){
            $domain = $this->checkDomain($url);
            if($domain){
                $links[$domain][$id] = $url;
            }
        }
        return $links;
    }

    public function checkDomain(String $url){
        $host = parse_url($url, PHP_URL_HOST);
        if($host && !empty($this->rules)){
            if($domain = array_search($host, $this->domains)){
                return $domain;
            }
            else{
                foreach ($this->rules as $name => $rule){
                    preg_match($rule, $host, $matches);
                    if(!empty($matches[0]) && $domain = array_search($matches[0], $this->domains)){
                        return $domain;
                    }
                }
            }
        }
        return false;
    }

}