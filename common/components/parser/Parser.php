<?php


namespace common\components\parser;

use yii\base\BaseObject;
use common\components\parser\classes\BaseParser;

class Parser extends BaseObject
{

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();
    }

    public function runParser(){
        $parser = new BaseParser;
        $parser->run();
    }

    public function runParserByUrl($url, $save){
        $parser = new BaseParser;
        $parser->runByUrl($url, $save);
    }

}