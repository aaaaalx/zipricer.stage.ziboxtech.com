<?php

namespace core\interfaces;


use core\entity\Notification;
use core\entity\NotificationsUsers;

interface NotificationRepositoryInterface
{
    public function save(Notification $notification);
    public function saveNotificationsUsers(NotificationsUsers $notificationsUsers);
    public function get($id):Notification;
}