<?php

namespace core\interfaces;


use core\entity\Project;

interface ProjectRepositoryInterface
{
    public function save(Project $project);
    public function get($id):Project;
}