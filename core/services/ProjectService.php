<?php
namespace core\services;


use core\forms\ProjectsForm;
use core\entity\Project;
use core\repositories\ProjectRepository;
use core\interfaces\ProjectRepositoryInterface;
use yii\web\UploadedFile;
use yii\helpers\BaseVarDumper;

class ProjectService
{

    private $repository;

    public function __construct(ProjectRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(ProjectsForm $form){
        $project = Project::create($form->name, $form->description, $form->vendor_code, $form->price, $form->user_id, $form->status);
        $this->repository->save($project);
        if($form->isFileAdded()){
            $path = $project->getUploadsPath($project->id);
            if($fileName = $form->uploadFile($path)){
                $project->photo = $fileName;
                $this->repository->save($project);
            }
        }
        return $project;
    }

    public function edit(ProjectsForm $form, $project_id)
    {
        $project = $this->repository->get($project_id);
        $project->edit($form->name, $form->description, $form->vendor_code, $form->price, $form->user_id, $form->status);
        if($form->isFileAdded()){
            $path = $project->getUploadsPath($project->id);
            if($fileName = $form->uploadFile($path)){
                $project->deleteFiles($project->oldAttributes['photo']);
                $project->photo = $fileName;
            }
        }
        else{
            if($form->photo == 'delete'){
                $project->photo = '';
                $project->deleteFiles();
            }
        }
        if($project->validate()){
            $this->repository->save($project);
        }
        else{
            print_r($project->errors);die;
        }
        return $project;
    }
}