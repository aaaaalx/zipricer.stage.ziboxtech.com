<?php
namespace core\services;

use Yii;
use core\forms\NotificationForm;
use core\entity\Notification;
use core\interfaces\NotificationRepositoryInterface;
use core\entity\NotificationsUsers;
use common\models\Profile;
use yii\db\Query;
use yii\helpers\BaseVarDumper;

class NotificationService
{

    private $repository;
    private $batchCount = 100;

    public function __construct(NotificationRepositoryInterface $repository){
        $this->repository = $repository;
    }

    public function create(NotificationForm $form){
        $notification = Notification::create($form->title, $form->message, $form->publication_at, $form->status);
        $this->repository->save($notification);
        if(in_array(0,$form->users)){
            $users = Profile::find()->where(['status'=>Profile::STATUS_ACTIVE]);
            $attributes = ['notification_id', 'profile_id', 'status'];
            foreach ($users->batch($this->batchCount) as $usersPack){
                if(!empty($usersPack)){
                    $rows = array_map(function($item)use($notification){
                        return [$notification->id, $item['id'], NotificationsUsers::ACTIVE];
                    }, $usersPack);
                    Yii::$app->db->createCommand()->batchInsert(NotificationsUsers::tableName(), $attributes, $rows)->execute();
                }
            }
        }
        elseif(!empty($form->users)){
            $users = Profile::find()
                ->where(['status'=>Profile::STATUS_ACTIVE])
                ->andWhere(['in', 'id', $form->users])->all();
            if(!empty($users)){
                foreach ($users as $user){
                    $notificationsUsers = NotificationsUsers::create($notification->id, $user->id);
                    $this->repository->saveNotificationsUsers($notificationsUsers);
                }
            }
        }
        return $notification;
    }

    public function edit(NotificationForm $form, $notification_id)
    {
        $notification = $this->repository->get($notification_id);
        $notification->edit($form->title, $form->message, $form->publication_at, $form->status);
        $this->repository->save($notification);
        if(!empty($form->users)){
            if($notification->publication_at > strtotime('now')){//if notification was't publicated
                NotificationsUsers::deleteAll(['notification_id'=>$notification->id]);
                if(in_array(0, $form->users)){
                    $users = (new Query())->from('profile')->where(['>','status', Profile::STATUS_DELETED])->orderBy('id');
                    foreach ($users->each() as $user){
                        $notificationsUsers = NotificationsUsers::create($notification->id, $user['id']);
                        $this->repository->saveNotificationsUsers($notificationsUsers);
                    }
                }
                else{
                    foreach ($form->users as $user){
                        $notificationsUsers = NotificationsUsers::create($notification->id, $user);
                        $this->repository->saveNotificationsUsers($notificationsUsers);
                    }
                }
            }
            else{//if notification publicated
                if(in_array(0, $form->users)){//if all users must be notified
                    $users = (new Query())->from('profile')->where(['>','status', Profile::STATUS_DELETED])->orderBy('id');
                    foreach ($users->batch() as $usersPack){
                        $usersPackIDs = array_column($usersPack, 'id');
                        $notificationsUsers = array_column(NotificationsUsers::find()
                            ->where(['in', 'profile_id', $usersPackIDs])
                            ->andWhere(['notification_id' => $notification->id])
                            ->all(), 'profile_id','id');
                        $result = array_diff($usersPackIDs, $notificationsUsers);
                        if(!empty($result)){
                            foreach ($result as $user_id){
                                $notificationsUsers = NotificationsUsers::create($notification->id, $user_id);
                                $this->repository->saveNotificationsUsers($notificationsUsers);
                            }
                        }
                    }
                }
                else{//if only selected users must be notified
                    NotificationsUsers::deleteAll(['notification_id'=>$notification->id]);
                    foreach ($form->users as $key => $user_id){
                        $notificationsUsers = NotificationsUsers::create($notification->id, $user_id);
                        $this->repository->saveNotificationsUsers($notificationsUsers);
                    }
                }
            }
        }
        return $notification;
    }
}