<?php


namespace core\repositories;

use core\entity\Notification;
use core\entity\NotificationsUsers;
use core\interfaces\NotificationRepositoryInterface;

class NotificationRepository implements NotificationRepositoryInterface
{
    public function save(Notification $notification){
        if(!$notification->save()){
            throw new \DomainException('Notification was not saved!');
        }
        return true;
    }

    public function saveNotificationsUsers(NotificationsUsers $notificationsUsers){
        if(!$notificationsUsers->save()){
            throw new \DomainException('Notifications users was not saved!');
        }
        return true;
    }

    public function get($id):Notification {
        return Notification::findOne($id);
    }

    public function deleteAll($id){
        return NotificationsUsers::deleteAll(['notification_id' => $id]);
    }

}