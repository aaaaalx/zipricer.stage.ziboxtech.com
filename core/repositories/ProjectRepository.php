<?php


namespace core\repositories;

use core\entity\Project;
use core\interfaces\ProjectRepositoryInterface;

class ProjectRepository implements ProjectRepositoryInterface
{
    public function save(Project $project){
        if(!$project->save()){
            throw new \DomainException('Project was not saved!');
        }
        return true;
    }

    public function get($id):Project {
        return Project::findOne($id);
    }

}