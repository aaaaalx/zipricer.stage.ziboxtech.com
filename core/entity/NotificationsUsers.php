<?php

namespace core\entity;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "notifications_users".
 *
 * @property int $id
 * @property int $notification_id
 * @property int $profile_id
 * @property int $status
 */
class NotificationsUsers extends ActiveRecord
{

    const ACTIVE = 1;
    const INACTIVE = 2;
    const DELETED = 0;

    public static function getStatuses(){
        return [
            self::ACTIVE => Yii::t('app', 'Active'),
            self::INACTIVE => Yii::t('app', 'Inactive'),
            self::DELETED => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notification_id', 'profile_id'], 'required'],
            [['notification_id', 'profile_id', ], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'notification_id' => Yii::t('app', 'Notification ID'),
            'profile_id' => Yii::t('app', 'Profile ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public static function create($notification_id, $profile_id)
    {
        $notificationsUsers = new self();
        $notificationsUsers->notification_id = $notification_id;
        $notificationsUsers->profile_id = $profile_id;
        $notificationsUsers->status = self::ACTIVE;
        return $notificationsUsers;
    }

    public function edit($notification_id, $profile_id)
    {
        $this->notification_id = $notification_id;
        $this->profile_id = $profile_id;
        $this->status = self::ACTIVE;
    }

    public function getNotification(){
        return $this->hasOne(Notification::class, ['id' => 'notification_id']);
    }

    public static function getUserNotifications($limit=-1){
        $notifications = self::find()->where(['profile_id'=>Yii::$app->user->id])->andWhere(['!=', 'status', self::DELETED])->limit($limit)->all();
        return is_array($notifications)?$notifications:[];
    }

    public static function setStatuses(array $notifications, $status){
        self::updateAll(['status'=>$status], ['in', 'id', array_column($notifications, 'id')]);
    }

    public static function getActiveNotificationsCount(){
        return self::find()->where(['profile_id'=>Yii::$app->user->id])->andWhere(['=', 'status', self::ACTIVE])->count();
    }

    public static function getNotificationsCount(){
        return self::find()->where(['profile_id'=>Yii::$app->user->id])->andWhere(['!=', 'status', self::DELETED])->count();
    }
}
