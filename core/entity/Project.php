<?php

namespace core\entity;

use common\models\LinksHistory;
use Yii;
use yii\db\ActiveRecord;
use common\models\Profile;
use common\models\ProjectsLinks;
use common\helpers\MainHelper;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property string $name
 * @property string $photo
 * @property string $description
 * @property string $vendor_code
 * @property string $price
 * @property int $user_id
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Project extends ActiveRecord
{
    const ACTIVE = 1;
    const IN_CONSIDERED = 2;
    const INACTIVE = 3;
    const DELETED = 0;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ]
        ];
    }

    public static function tableName()
    {
        return '{{%projects}}';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'photo' => Yii::t('app', 'Photo'),
            'description' => Yii::t('app', 'Description'),
            'vendor_code' => Yii::t('app', 'Vendor Code'),
            'price' => Yii::t('app', 'Price'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public static function getStatuses($status=null){
        $statuses = [
            self::ACTIVE => 'Active',
            self::IN_CONSIDERED => 'In considered',
            self::INACTIVE => 'Inactive',
            self::DELETED => 'Deleted'
        ];
        if(isset($status)){
            return $statuses[$status];
        }
        return $statuses;
    }

    public static function create($name, $description, $vendor_code, $price, $user_id, $status = self::ACTIVE)
    {
        $project = new self();
        $project->name = $name;
        $project->description = $description;
        $project->vendor_code = $vendor_code;
        $project->price = $price;
        $project->user_id = $user_id;
        $project->status = $status;
        return $project;
    }

    public function edit($name, $description, $vendor_code, $price, $user_id, $status = self::ACTIVE)
    {
        $this->name = $name;
        $this->description = $description;
        $this->vendor_code = $vendor_code;
        $this->price = $price;
        $this->user_id = $user_id;
        $this->status = $status;
    }

    public function getUploadsUrl($dir=null, $fileName=null){
        $dir = $dir?$dir:$this->id;
        $fileName = $fileName?$fileName:$this->photo;
        if(!$dir || !$fileName){
            return null;
        }
        return Yii::$app->params['frontendHost'].'uploads/projects/'.$dir.'/'.$fileName;
    }

    public function getUploadsPath($dir=null){
        $dir = $dir?$dir:$this->id;
        if(!$dir){
            return null;
        }
        return Yii::getAlias('@uploads').'\projects\\'.$dir.'\\';
    }

    public function getUser(){
        return $this->hasOne(Profile::class, ['id' => 'user_id']);
    }

    public function getLinks(){
        return $this->hasMany(ProjectsLinks::class, ['project_id' => 'id'])->from('projects_links AS links');
    }

    public function deleteFiles($name=null,$removeAll=false){
        $path = $this->getUploadsPath();
        if(is_dir($path)){
            if($name && file_exists($path.$name)){
                unlink($path.$name);
                MainHelper::remove_if_dir_empty($path);
            }
            elseif($removeAll){
                MainHelper::remove_dir($path);
            }
        }
    }

    public function filterLinksByStatuses($count = false, $condition, array $statuses){
        $condition = $condition?'in':'not in';
        $ProjectsLinksQuery = ProjectsLinks::find()->where(['project_id'=>$this->id])->andWhere([$condition, 'link_status', $statuses]);
        return $count?$ProjectsLinksQuery->count():$ProjectsLinksQuery->all();
    }

    public function getLinksCount($condition, array $statuses){
        $condition = $condition?'in':'not in';
        return ProjectsLinks::find()->where(['project_id'=>$this->id])->andWhere([$condition, 'link_status', $statuses])->count();
    }

    public function getHigherPrices(){
        $statuses = [ProjectsLinks::INACTIVE,ProjectsLinks::DELETED];
        $linksHistoryQuery = LinksHistory::find()->where(['>','price', $this->price])->joinWith(['link.links'=>function($query)use($statuses){
            return $query->where(['project_id'=>$this->id])->andWhere(['not in', 'link_status', $statuses]);
        }])->groupBy(['link_id']);

        return $linksHistoryQuery->count();
    }

    public function getHistory($link_id=null,$asQuery=false,$offset=0,$limit=null,$statuses=[ProjectsLinks::DELETED]){
        $linksHistoryQuery = LinksHistory::find()->joinWith(['link.links'=>function($query)use($statuses, $link_id){
            $params = ['projects_links.project_id'=>$this->id];
            $link_id?$params += ['projects_links.link_id'=>$link_id]:false;
            return $query->where($params)->andWhere(['not in', 'projects_links.link_status', $statuses]);
        }])->where(['not in', 'links_history.status', [LinksHistory::DELETED]])->orderBy(['links_history.created_at'=>SORT_DESC])->offset($offset)->limit($limit);

        return $asQuery?$linksHistoryQuery:$linksHistoryQuery->all();
    }

    public function getHistoryCount(){
        $statuses = [ProjectsLinks::INACTIVE,ProjectsLinks::DELETED];
        $linksHistoryQuery = LinksHistory::find()->joinWith(['link.links'=>function($query)use($statuses){
            return $query->where(['projects_links.project_id'=>$this->id])->andWhere(['not in', 'link_status', $statuses])->orderBy(['links.id'=>SORT_ASC]);
        }])->groupBy(['link_id']);

        return $linksHistoryQuery->count();
    }

}