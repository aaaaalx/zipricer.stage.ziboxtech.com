<?php

namespace core\entity;

use common\models\Profile;
use Yii;
use yii\db\ActiveRecord;
use core\entity\NotificationsUsers;

/**
 * This is the model class for table "notifications".
 *
 * @property int $id
 * @property string $title
 * @property string $message
 * @property int $status
 * @property int $publication_at
 * @property int $created_at
 * @property int $updated_at
 */
class Notification extends ActiveRecord
{

    const ACTIVE = 1;
    const INACTIVE = 2;
    const DELETED = 0;

    public static function getStatuses(){
        return [
            self::ACTIVE => Yii::t('app', 'Active'),
            self::INACTIVE => Yii::t('app', 'Inactive'),
            self::DELETED => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at' ]
                ],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'message' => Yii::t('app', 'Message'),
            'status' => Yii::t('app', 'Status'),
            'publication_at' => Yii::t('app', 'Publication At'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public static function create($title, $message, $publication_at = null, $status = self::ACTIVE)
    {
        $notification = new self();
        $notification->title = $title;
        $notification->message = $message;
        $notification->status = $status;
        $notification->publication_at = $publication_at?strtotime($publication_at):strtotime('now');
        return $notification;
    }

    public function edit($title, $message, $publication_at, $status = self::ACTIVE)
    {
        $this->title = $title;
        $this->message = $message;
        $this->status = $status;
        $this->publication_at = strtotime($publication_at);
    }

    public function getNotificationsUsers()
    {
        return $this->hasMany(Profile::class, ['id' => 'profile_id'])->viaTable('notifications_users', ['notification_id' => 'id']);
    }

    public function getNotificationsUsersItems(){
        return $this->hasMany(NotificationsUsers::class, ['notification_id' => 'id']);
    }

}
