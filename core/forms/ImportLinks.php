<?php

namespace core\forms;

use yii\base\Model;

class ImportLinks extends Model
{

    public $file;
    public $project_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'csv', 'checkExtensionByMimeType' => false, 'maxSize' => 5120000],
            ['project_id', 'required'],
            ['project_id', 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'file' => \Yii::t('app','File'),
        ];
    }

}