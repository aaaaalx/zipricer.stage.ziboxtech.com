<?php

namespace core\forms\frontend;

use Yii;
use yii\base\Model;
use common\models\ProjectsLinks;
use common\models\Links;

/**
 * This is the model class for table "Links".
 *
 * @property array $links
 */
class LinksForm extends Model
{
    public $url;
    public $status;
    public $project_id;

    public function __construct(ProjectsLinks $links=null, array $config = [])
    {
        parent::__construct($config);
        $this->url = $links->link->link_url;
        $this->status = $links->link_status;
        $this->project_id = $links->project_id;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'status'], 'required'],
            ['url', 'url'],
            [['status', 'project_id'], 'integer'],
            ['url', function($attribute, $params){
                $link = Links::findOne(['link_url'=>$this->{$attribute}]);
                if($link){
                    if(ProjectsLinks::findOne(['link_id'=>$link->id, 'project_id'=>$this->project_id])){
                        $this->addError($attribute, Yii::t('app', 'Url must be unique!'));
                    }
                }
            }]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'links' => Yii::t('app', 'Links'),
        ];
    }

}
