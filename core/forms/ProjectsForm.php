<?php

namespace core\forms;

use core\entity\Project;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use common\helpers\MainHelper;
use yii\helpers\BaseVarDumper;

/**
 * This is the model class for table "projects".
 *
 * @property string $name
 * @property string $photo
 * @property string $description
 * @property string $vendor_code
 * @property integer $price
 * @property integer $user_id
 */
class ProjectsForm extends Model
{

    public $id;
    public $name;
    public $photo;
    public $description;
    public $vendor_code;
    public $price;
    public $user_id;
    public $status;

    private $project;

    public function __construct(Project $project=null, array $config = [])
    {
        parent::__construct($config);
        if($project){
            $this->id = $project->id;
            $this->name = $project->name;
            $this->photo = $project->photo;
            $this->description = $project->description;
            $this->vendor_code = $project->vendor_code;
            $this->price = $project->price;
            $this->user_id = $project->user_id;
            $this->status = $project->status;
            $this->project = $project;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['photo', 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'minWidth' => 100, 'maxWidth' => 1000, 'minHeight' => 100, 'maxHeight' => 1000,],
            [['name'], 'required'],
            [['id', 'price', 'user_id', 'status'], 'integer'],
            [['name', 'vendor_code'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
            ['user_id', 'exist', 'targetClass' => 'common\models\Profile', 'targetAttribute' => 'id'],
            [['name'], 'unique', 'targetClass' => 'core\entity\Project', 'targetAttribute' => ['id', 'name', 'user_id'], 'filter' => function($query){return $query->where(['!=','id',$this->id])->andWhere(['name'=>$this->name])->andWhere(['user_id'=>$this->user_id]);}],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app','Name'),
            'photo' => Yii::t('app','Photo'),
            'description' => Yii::t('app','Description'),
            'vendor_code' => Yii::t('app','Vendor Code'),
            'price' => Yii::t('app','Price'),
            'user_id' => Yii::t('app','User ID'),
            'status' => Yii::t('app','Status')
        ];
    }

    public function uploadFile($path){
        $file = MainHelper::setUniqueFileName($this->photo->baseName, $this->photo->extension);
        if(!is_dir($path)){
            mkdir($path, 0777, true);
        }
        if($this->photo->saveAs($path.$file)){
            return $file;
        }
        return false;
    }

    public function isFileAdded(){
        return $this->photo instanceof UploadedFile;
    }

}
