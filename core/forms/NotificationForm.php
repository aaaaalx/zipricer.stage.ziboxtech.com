<?php

namespace core\forms;

use common\models\Profile;
use core\entity\Notification;
use Yii;
use yii\base\Model;
use core\entity\NotificationsUsers;

/**
 * This is the model class for table "notifications".
 *
 * @property string $title
 * @property string $message
 * @property int $status
 * @property string $publication_at
 */
class NotificationForm extends Model
{
    public $title;
    public $message;
    public $status;
    public $publication_at;
    public $users;

    public function __construct(Notification $notification = null, array $config = [])
    {
        parent::__construct($config);
        if($notification){
            $this->title = $notification->title;
            $this->message = $notification->message;
            $this->status = $notification->status;
            $this->publication_at = date('Y-m-d H:i', $notification->publication_at);
            $this->users = array_column($notification->notificationsUsers,'username', 'id');
            $usersCount = Profile::find()->where(['>','status', Profile::STATUS_DELETED])->count();
            if(count($this->users)==$usersCount){
                $this->users = [0=>'<b> -- '.Yii::t('app', 'Select all').' -- </b>'];
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'status', 'users'], 'required'],
            [['status', 'publication_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['message'], 'string', 'max' => 2000],
            [['users'], 'each', 'rule' => ['integer']],
            ['publication_at', 'default', 'value' => function($model){
                empty($model->publication_at)?strtotime('now'):$model->publication_at;
            }]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'publication_at' => Yii::t('app', 'Publication'),
            'users' => Yii::t('app', 'Users'),
        ];
    }

}