<?php
namespace core\validators;

use Yii;
use yii\validators\Validator;

class CheckAllUsersByUsersFieldsValidator extends Validator{
    public function init() {
        parent::init ();
        $this->message = Yii::t('app', 'If users not added, checkbox "All users" must be checked!');
    }

    public function validateAttribute($model, $attribute) {
        if ($attribute == 'all_users' && empty(trim($model->users)) && $model->all_users != 1) {
            $model->addError($attribute, $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view){

        $errors = json_encode($model->getErrors(), true);

        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $form_id = basename(get_class($model)).'_form';
        if($attribute == 'all_users'){
            return <<<JS
            if (!$("#notificationform-all_users").is( ":checked" ) && !$("#notificationform-users").val()){
                messages.push($message);
            }
JS;
        }
        elseif($attribute == 'users'){
            return <<<JS
            if (!$("#notificationform-all_users").is( ":checked" ) && $("#notificationform-users").val()){
                $("#$form_id").yiiActiveForm('updateAttribute', 'notificationform-all_users', '');
            }
            else if(!$("#notificationform-all_users").is( ":checked" ) && !$("#notificationform-users").val()){
                $("#$form_id").yiiActiveForm('updateAttribute', 'notificationform-all_users', $message);
            }
JS;
        }
    }
}