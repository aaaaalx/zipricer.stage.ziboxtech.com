<?php

namespace frontend\controllers;

use common\components\parser\classes\BaseParser;
use common\helpers\MainHelper;
use common\models\Links;
use common\models\LinksForm;
use core\entity\Project;
use core\forms\ImportLinks;
use Yii;
use common\models\ProjectsLinks;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\helpers\BaseVarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * ProjectLinksController implements the CRUD actions for ProjectsLinks model.
 */
class ProjectLinksController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProjectsLinks models.
     * @param integer $project_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionIndex($project_id=null)
    {
        if(!empty($project_id)){
            $projectQuery = Project::find()->where(['id'=>$project_id, 'user_id'=>Yii::$app->user->id])->andWhere(['!=', 'status', Project::DELETED]);
            if($project = $projectQuery->one()){

                $link = new LinksForm();

                $statistic = [];
                $statistic['totalLinks'] = $project->getLinksCount(false, [ProjectsLinks::DELETED]);
                $statistic['totalActiveLinks'] = $project->filterLinksByStatuses(true, true, [ProjectsLinks::ACTIVE]);
                $statistic['nextParsingData'] = date('Y.m.d', strtotime('now'));//need to get real parsing data!!!!
                $statistic['competitorsHigherPrices'] = $project->getHigherPrices();

                $pages = new Pagination([
                    'pageSize'=>10,
                    'totalCount' => $statistic['totalLinks'],
                ]);

                $dataProvider = new ActiveDataProvider([
                    'query' => $project->getLinks()->where(['!=', 'link_status', ProjectsLinks::DELETED])->orderBy(['created_at'=>SORT_DESC])->offset($pages->offset)->limit($pages->limit),
                    'pagination' => false,
                    'sort' => [
                        'defaultOrder' => [
                            'created_at' => SORT_DESC,
                        ]
                    ],
                ]);

                $importForm = new ImportLinks();

                return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'project' => $project,
                    'statistic' => $statistic,
                    'pages' => $pages,
                    'link' => $link,
                    'file' => $importForm
                ]);
            }
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionImport(){
        $importForm = new ImportLinks();
        if($importForm->load(Yii::$app->request->post())){
            $file = UploadedFile::getInstance($importForm, 'file');
            $importForm->file = $file;
            if($importForm->validate() && !empty($file->tempName)){
                $fileContent = MainHelper::readCsv($file->tempName);
                if(!empty($fileContent)){
                    $maxCount = 100;
                    if(count($fileContent)>$maxCount){
                        \Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Reached maximum count of urls').': '.$maxCount.'!');
                        return $this->redirect(['project-links/index', 'project_id'=>$importForm->project_id]);
                    }
                    $post = [];
                    foreach ($fileContent as $url){
                        $post['LinksForm'][] = [
                            'url' => $url,
                            'status' => ProjectsLinks::ACTIVE,
                            'project_id' => $importForm->project_id
                        ];
                    }
                    $this->actionCreate($importForm->project_id, $post);
                }
            }
        }
    }

    /**
     * Displays a single ProjectsLinks model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProjectsLinks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $project_id
     * @param array $post
     * @return mixed
     */
    public function actionCreate($project_id, $post = [])
    {
        $post = $post?:Yii::$app->request->post();
        $links = [new LinksForm()];
        $count = count(@$post['LinksForm']);
        for($i = 1; $i < $count; $i++) {
            $links[] = new LinksForm();
        }
        if(Model::loadMultiple($links, $post) && Model::validateMultiple($links)){
            $domains = BaseParser::getDomains();
            $urls = array_column($links, 'url');
            $LinksModels = array_column(Links::find()->where(['in', 'link_url', $urls])->all(), null, 'id');
            $existingLinks = array_column($LinksModels,'link_url', 'id');
            $errors = [
                'domains'=>[],
                'links'=>[],
                'projectLinks'=>[]
            ];
            foreach ($links as $model){
                $url = parse_url($model->url);
                $domain = @$url['host'];
                if(!in_array($domain, $domains)){
                    //отправить запрос если нет домена в списке
                    $errors['domains'][] = Yii::t('app', "$domain is not supported!");
                }
                else{
                    if($id = array_search($model->url, $existingLinks)){
                        $ProjectsLinks = new ProjectsLinks();
                        $ProjectsLinks->link_id = $id;
                        $ProjectsLinks->project_id = $project_id;
                        $ProjectsLinks->link_status = $model->status;
                        if(!$ProjectsLinks->save()){
                            $errors['projectLinks'][] = Yii::t('app', "$model->url was not added to project!").' ('.$ProjectsLinks->errors['link_id'][0].')';
                        }
                    }
                    else{
                        if(!$links = Links::findOne(['link_url'=>$model->url])){
                            $links = new Links();
                            $links->link_url = $model->url;
                            $links->status = 1;
                        }
                        if($links->save()){
                            $ProjectsLinks = new ProjectsLinks();
                            $ProjectsLinks->link_id = $links->id;
                            $ProjectsLinks->project_id = $project_id;
                            $ProjectsLinks->link_status = $model->status;
                            if(!$ProjectsLinks->save()){
                                $errors['projectLinks'][] = Yii::t('app', "$model->url was not added to project!").' ('.$ProjectsLinks->errors['link_id'][0].')';
                            }
                        }
                        else{
                            $errors['links'][] = Yii::t('app', "$model->url was not created!");
                        }
                    }
                }
            }
            if(!empty($errors['domains']) || !empty($errors['links']) || !empty($errors['projectLinks'])){
                $message = implode(' <br>', $errors['domains']).implode(' <br>', $errors['links']).implode(' <br>', $errors['projectLinks']);
                \Yii::$app->getSession()->setFlash('error',  $message);
                if(count($urls)>count($errors['domains'])&&count($urls)>count($errors['links'])&&count($urls)>count($errors['projectLinks'])){
                    \Yii::$app->getSession()->setFlash('success',  Yii::t('app', 'All rest valid links was added!'));
                }
            }
            else{
                \Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Links was successfully saved!'));
            }
        }
        else{
            $cleaned = [];
            $errors = '';
            foreach ($links as $link){
                if(!empty($link->errors)){
                    foreach ($link->errors as $key => $field){
                        $mess = implode(', ', $field);
                        $errors .= $key.': '.$link['url'].' - '.$mess.' <br>';
                    }
                    \Yii::$app->getSession()->setFlash('error', Yii::t('app', $errors));
                }
                else{
                    $cleaned['LinksForm'][] = [
                        'url' => $link->url,
                        'status' => $link->status,
                        'project_id' => $link->project_id
                    ];
                }
            }
            if(!empty($cleaned)){
                $this->actionCreate($project_id, $cleaned);
            }
        }
        return $this->redirect(['project-links/index', 'project_id' => $project_id]);
    }

    /**
     * Deletes an existing ProjectsLinks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model && $model->project){
            if($this->isOwner($model->project->user_id)){
                if($model->delete()){
                    \Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Links was deleted!'));
                }
                else{
                    \Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Links was not deleted!'));
                }
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function isOwner($id){
        return $id == Yii::$app->user->id;
    }

    /**
     * Finds the ProjectsLinks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProjectsLinks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProjectsLinks::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
