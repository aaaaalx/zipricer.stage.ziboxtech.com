<?php

namespace frontend\controllers;

use core\entity\Project;
use core\forms\ProjectsForm;
use core\services\ProjectService;
use Yii;
use yii\base\Module;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ProjectsController implements the CRUD actions for Project model.
 */
class ProjectsController extends Controller
{

    private $service;

    public function __construct($id, Module $module, array $config = [], ProjectService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Project::find()->where('status >= 1 and user_id = '.Yii::$app->user->id);
        $countQuery = clone $query;
        $pages = new Pagination([
            'pageSize'=>20,
            'totalCount' => $countQuery->count(),
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query->offset($pages->offset)->limit($pages->limit),
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'pages' => $pages,
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $projectsForm = new ProjectsForm();
        if($projectsForm->load(Yii::$app->request->post())){
            $projectsForm->photo = UploadedFile::getInstance($projectsForm, 'photo');
            $projectsForm->user_id = Yii::$app->user->id;
            if($projectsForm->validate()){
                $this->service->create($projectsForm);
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $projectsForm,
        ]);
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $project = $this->findModel($id);
        if(!$this->isOwner($project->user_id)){
            return $this->redirect(['index']);
        }
        $projectsForm = new ProjectsForm($project);
        if($projectsForm->load(Yii::$app->request->post())) {
            $projectsForm->photo = UploadedFile::getInstance($projectsForm, 'photo');
            $projectsForm->user_id = Yii::$app->user->id;
            $projectsForm->id = $project->id;
            if ($projectsForm->validate()){
                $this->service->edit($projectsForm, $project->id);
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $projectsForm,
            'project' => $project
        ]);
    }

    public function isOwner($id){
        return $id == Yii::$app->user->id;
    }

    public function actionRemoveImage($id){
        if(Yii::$app->request->isAjax){
            $project = $this->findModel($id);
            if($this->isOwner($project->user_id)){
                $project->photo = null;
                $project->deleteFiles($project->oldAttributes['photo']);
                return $project->save();
            }
        }
        return false;
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $project = $this->findModel($id);
        if($this->isOwner($project->user_id)){
            $project->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
