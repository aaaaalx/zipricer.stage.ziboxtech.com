<?php

namespace frontend\controllers;

use core\entity\NotificationsUsers;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ProjectsController implements the CRUD actions for Project model.
 */
class NotificationsController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = NotificationsUsers::find()->where(['notifications_users.profile_id'=>Yii::$app->user->id])->andWhere(['!=', 'notifications_users.status', NotificationsUsers::DELETED])->joinWith('notification', true, 'INNER JOIN');

        $notificationsQuery = clone $query;
        $pages = new Pagination([
            'pageSize'=>20,
            'totalCount' => $notificationsQuery->count(),
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query->offset($pages->offset)->orderBy(['notifications.created_at'=>SORT_DESC])->limit($pages->limit),
            'pagination' => false
        ]);

        $notifications = $dataProvider->getModels();

        NotificationsUsers::setStatuses($notifications, NotificationsUsers::INACTIVE);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'pages' => $pages,
            'notifications' => is_array($notifications)?$notifications:[]
        ]);
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id){
        $model = $this->findModel($id);
        if($model->profile_id == Yii::$app->user->id){
            $model->delete();
        }
        return $this->redirect(['notifications/index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NotificationsUsers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
