<?php

namespace frontend\controllers;

use Yii;
use common\models\Profile;
use common\models\ProfileSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\SignupForm;
use yii\web\UploadedFile;

/**
 * ProfileController implements the CRUD actions for Profile model.
 */
class ProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Profile models.
     * @return mixed
     */
    public function actionIndex()
    {
        $id = Yii::$app->user->id;
        $profile = $this->findModel($id);
        $model = new SignupForm($profile);

        if ($model->load(Yii::$app->request->post())) {
            $model->icon = UploadedFile::getInstance($model, 'icon');
            if($model->validate()){
                $profile->edit($model->username, $model->email, $model->status, $model->referral_user_id, $model->referral_link);
                if($profile->user->edit($model->email, $model->password) && !$profile->user->save()){
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Profile not updated!'));
                    return $this->redirect('/profile/index');
                }
                if($model->isFileAdded()){
                    $path = $profile->getUploadsPath($profile->id);
                    if($fileName = $model->uploadFile($path)){
                        $profile->icon = $fileName;
                    }
                }
                if ($profile->save()) {
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Profile updated!'));
                    return $this->redirect('/profile/index');
                }
            }
        }

        return $this->render('index', [
            'profile' => $profile,
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing Profile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Profile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Profile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profile::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
