<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProjectsLinks */

$this->title = Yii::t('app', 'Create Projects Links');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-links-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
