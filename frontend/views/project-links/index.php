<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects Links');
$this->params['breadcrumbs'][] = $this->title;

$scripts = [];
?>

<section class="content-header content-header-own full">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb breadcrumb-own">
                <li><a href="<?= Url::to(['site/dashboard']); ?>"><?= Yii::t('app', 'Dashboard') ?></a></li>
                <li><a href="<?= Url::to(['projects/index']) ?>">Projects</a></li>
                <li class="active">Project: <?= Html::encode($project->name) ?></li>
            </ol>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="white-box">
                <div class="project-detail-wrap">
                    <h2><?= Html::encode($project->name) ?></h2>
                    <hr>
                    <div class="project-action-box">
                        <a href="<?= Url::to(['projects/update', 'id'=>$project->id]) ?>" class="btn btn-default btn-tool orange"><i class="fa fa-pencil"></i><?= Yii::t('app', 'edit project') ?></a>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-4 col-sm-6">
                            <div class="project-item-img">
                                <?= $project->photo?Html::img($project->getUploadsUrl($project->id, $project->photo), ['style'=>'max-width:240px; max-height:300px;']):''; ?>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-8 col-sm-6">
                            <div class="row">
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-aqua small-box-project-detail">
                                        <div class="inner">
                                            <h3><?= $statistic['totalLinks'] ?></h3>
                                            <p><?= Yii::t('app', 'Total Links') ?></p>
                                        </div>
                                        <div class="icon icon-65">
                                            <i class="fa fa-shopping-cart"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-green small-box-project-detail">
                                        <div class="inner">
                                            <h3><?= $statistic['totalActiveLinks'] ?></h3>
                                            <p><?= Yii::t('app', 'Total parcing') ?></p>
                                        </div>
                                        <div class="icon icon-65">
                                            <i class="ion ion-stats-bars"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-yellow small-box-project-detail">
                                        <div class="inner">
                                            <h3><?= $statistic['nextParsingData'] ?></h3>
                                            <p><?= Yii::t('app', 'Next automatic parsing') ?></p>
                                        </div>
                                        <div class="icon icon-65">
                                            <i class="ion ion-person-add"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-red small-box-project-detail">
                                        <div class="inner">
                                            <h3><?= $statistic['competitorsHigherPrices'] ?></h3>
                                            <p><?= Yii::t('app', 'Competitors have higher prices') ?></p>
                                        </div>
                                        <div class="icon icon-65">
                                            <i class="ion ion-pie-graph"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="project-item-description">
                                        <p><?= $project->description ?></p>
                                    </div>
                                    <div class="your-price-box">
                                        <?= Yii::t('app', 'Your price now') ?>: <span>$<?= (int)$project->price ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="white-box">
                <div class="table-tools">
                    <div class="row">
                        <div class="col-xs-6">

                            <?php $form = ActiveForm::begin(['action'=>Url::to(['project-links/create', 'project_id'=>$project->id]), 'options' => ['class' => 'form-url-upload']]); ?>

                                <div class="col-xs-10">
                                    <?= $form->field($link, "[0]url")->textInput(['id'=>'inputUrl', 'class'=>'form-control form-control-own', 'placeholder'=>'http://'])->label(false) ?>
                                    <?= $form->field($link, "[0]project_id", ['template' => '{input}{error}'])->hiddenInput(['value'=>$project->id])->label(false) ?>
                                    <?= $form->field($link, "[0]status", ['template' => '{input}{error}'])->hiddenInput(['value'=>\common\models\ProjectsLinks::ACTIVE])->label(false) ?>
                                </div>
                                <div class="col-xs-2">
                                    <div class="form-group to-left">
                                        <?= Html::submitButton(Yii::t('app', 'create url'), ['class' => 'btn btn-default btn-add-link']) ?>
                                    </div>
                                </div>

                            <?php ActiveForm::end(); ?>

                            <button type="button" class="btn btn-default btn-tool blue" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-cloud-upload"></i>import</button>
                            <!--<button type="button" class="btn btn-default btn-tool orange"><i class="fa fa-cloud-download"></i>export</button>-->

                        </div>

                        <div class="col-sm-6">
                            <nav aria-label="Page navigation">
                                <?php
                                echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pages,
                                    'options' => [
                                        'class' => 'pagination box-tools-navigation pagination-own',
                                    ],
                                    'prevPageLabel' => '<span><</span>',
                                    'nextPageLabel' => '<span>></span>',
                                ]);
                                ?>
                            </nav>
                        </div>

                    </div>
                </div>

                <table class="table table-project-links">
                    <thead>
                    <tr>
                        <th class="align-left">
                            Company Name
                        </th>
                        <th>
                            Last Parse
                        </th>
                        <th>
                            Price Price
                        </th>
                        <th>
                            Price difference
                        </th>
                        <th>
                            Discount
                        </th>
                        <th>
                            Availability
                        </th>
                        <th>
                            Last Change
                        </th>
                        <th>
                            Link status
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $items = $dataProvider->query->all(); ?>

                    <?php foreach ($items as $num => $item): ?>

                        <tr>
                            <td class="align-left">
                                <a href="#" class="project-company-name">
                                    <?php
                                    $lastParse = $item->link->getLastUpdate();
                                    if($lastParse){
                                        echo Html::encode($lastParse->link_name);
                                    }
                                    else{
                                        echo Html::encode($item->link->link_url);
                                    }
                                    ?>
                                </a>
                            </td>
                            <td>
                                <?php
                                $lastParse = $item->link->getLastUpdate();
                                if($lastParse) {
                                    echo date('Y.m.d', $lastParse->created_at);
                                }
                                else{
                                    echo ' - ';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                $lastParse = $item->link->getLastUpdate();
                                if($lastParse) {
                                    echo Html::encode($lastParse->price);
                                }
                                else{
                                    echo ' - ';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                $lastParse = $item->link->getLastUpdate();
                                if($lastParse) {
                                    $price = (int)$lastParse->price;
                                    $projectPrice = (int)$project->price;
                                    $res = $projectPrice-$price;
                                    $price = $price>$projectPrice?'<span style="color:#11bd6e;font-weight:bold;">'.$res.' <i class="fa fa-level-down"></i></span>':'<span style="color:#ff0000;font-weight:bold;">+'.$res.' <i class="fa fa-level-up"></i>';
                                    echo $price;
                                }
                                else{
                                    echo ' - ';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                $lastParse = $item->link->getLastUpdate();
                                if($lastParse) {
                                    echo Html::encode($lastParse->discount);
                                }
                                else{
                                    echo ' - ';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                $lastParse = $item->link->getLastUpdate(false);
                                if($lastParse) {
                                    $availabilities = $lastParse->availabilities;
                                    $availability = (int)$lastParse->availability;
                                    $availabilitiesHtml = [
                                        0 => '<span class="label label-danger">not available</span>',
                                        1 => '<span class="label label-success">available</span>',
                                    ];
                                    $res = $availabilitiesHtml[0];
                                    if (in_array($availability, $availabilities)) {
                                        $res = $availabilitiesHtml[$availability];
                                    }
                                    echo $res;
                                }
                                else{
                                    echo ' - ';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                $lastParse = $item->link->getLastUpdate();
                                if($lastParse) {
                                    echo date('Y.m.d', $lastParse->end_time);
                                }
                                else{
                                    echo ' - ';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                $statusesHtml = [
                                    1=>'<i class="fa fa-check green-text"></i>',
                                    2=>'<i class="fa fa-close red-text"></i>'
                                ];
                                $res = $statusesHtml[2];
                                isset($statusesHtml[$item->link_status])?$res = $statusesHtml[$item->link_status]:false;
                                echo $res;
                                ?>
                            </td>
                            <td>
                                <a class="project-item-tool show-project-info" data-toggle="tooltip" title="Show more info"><i class="fa  fa-area-chart"></i></a>
                                <?= Html::a('<i class="fa fa-trash-o"></i>', ['project-links/delete', 'id'=>$item->id],
                                    [
                                        'title' => 'Delete',
                                        'class' => 'project-item-tool',
                                        'data' => [
                                            'method' => 'post',
                                            'data-toggle' => 'tooltip',
                                        ],
                                    ]
                                );?>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="10">
                                <div class="project-link-info">
                                    <div class="project-link-header">
                                        <a href="#">
                                            <?php
                                            $lastParse = $item->link->getLastUpdate();
                                            if($lastParse) {
                                                echo $lastParse->link->link_url;
                                            }
                                            else{
                                                echo ' - ';
                                            }
                                            $history = $project->getHistory($item->link->id);
                                            ?>
                                        </a>
                                    </div>
                                    <!--project-link chart-->
                                    <div class="box box-info box-project-link">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Line Chart</h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                </button>
                                                <button type="button" class="btn btn-box-tool " data-widget="remove"><i class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body chart-responsive">
                                            <div class="chart" id="line-chart-<?= $num+1; ?>"></div>
                                            <?php
                                            $historyChart = array_reverse($history);
                                            foreach ($historyChart as $key => $historyItem){
                                                isset($scripts[$num+1])?:$scripts[$num+1]=[];
                                                array_push($scripts[$num+1], ['year' => date('d.m.Y h:i:s', $historyItem->created_at), 'price' => $historyItem->price]);
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <!--project-link chart-->
                                    <!--project-link status-info-->
                                    <div class="box box-info box-project-link">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Product statistics</h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                </button>
                                                <button type="button" class="btn btn-box-tool " data-widget="remove"><i class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <table class="table table-product-status">

                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Parse Date</th>
                                                    <th>Price</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?php foreach ($history as $key => $historyItem): ?>
                                                <tr>
                                                    <td><?= ++$key; ?></td>
                                                    <td><?= date('d-m-Y', $historyItem->created_at); ?></td>
                                                    <td><?= $historyItem->price; ?></td>
                                                    <td>
                                                    <?php
                                                    $res = $availabilitiesHtml[0];
                                                    if(in_array($historyItem->availability, $availabilities)){
                                                        $res = $availabilitiesHtml[$historyItem->availability];
                                                    }
                                                    echo $res;
                                                    ?>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>

                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                    <!--project-link status-info-->
                                </div>
                            </td>
                        </tr>


                    <?php endforeach; ?>

                    </tbody>
                </table>

                <div class="table-tools">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="tools-counter">
                                Showing <?= $dataProvider->count; ?> of <?= $pages->totalCount; ?> entries
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <nav aria-label="Page navigation">
                                <?php
                                echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pages,
                                    'options' => [
                                        'class' => 'pagination box-tools-navigation pagination-own',
                                    ],
                                    'prevPageLabel' => '<span><</span>',
                                    'nextPageLabel' => '<span>></span>',
                                ]);
                                ?>
                            </nav>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
        var chartsData = <?= json_encode($scripts, true); ?>;
    </script>
    <!--begin modal-->
    <div class="modal modal-own fade" id="modal-upload">
        <div class="modal-dialog">
            <div class="modal-content modal-content-own rounded">
                <div class="modal-header modal-header-own">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title modal-title-own">Upload file with url</h4>
                </div>
                <div class="modal-body">

                    <?php $form = ActiveForm::begin(['action'=>Url::to(['project-links/import']), 'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-file-upload']]); ?>

                        <?= $form->field($file, "file", ['template' => '<div class="form-group"><span class="btn btn-default btn-file form-control-own">choose file{input}{error}</span></div>'])->fileInput(['id'=>'exampleInputFile', 'class'=>'form-control form-control-file'])->label(false) ?>
                        <?= $form->field($file, "project_id", ['template' => '{input}{error}'])->hiddenInput(['value'=>$project->id])->label(false) ?>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', 'ok'), ['class' => 'btn btn-default btn-add-file']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <!--end modal-->
</section>

