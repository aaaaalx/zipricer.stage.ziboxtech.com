<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model core\entity\Project */

$this->title = Yii::t('app', 'Update project');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content-header content-header-own full">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb breadcrumb-own">
                <li><a href="/">Dashboard</a></li>
                <li><a href="/projects">Projects</a></li>
                <li class="active">Update Project</li>
            </ol>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xl-6 col-xl-offset-3 col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            <div class="white-box">


                <?php $form = ActiveForm::begin(['options' => ['class' => 'form-create-project form-dark']]); ?>

                <div class="row">
                    <div class="col-xs-6">

                        <?= $form->field($model, 'name')->textInput(['id' => 'newProjectName', 'class'=> 'form-control form-control-own'])->label(Yii::t('app', 'Project Name')) ?>

                        <?= $form->field($model, 'description')->textarea(['id' => 'newProjectDesc', 'class'=> 'form-control form-control-own char-textarea textarea-sm'])->label(Yii::t('app', 'Project Description')) ?>

                        <div class="form-group">
                            <label for="newVendorCode">
                                Vendor Code
                                <sup class="pe-help" data-toggle="tooltip" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit natus beatae architecto">
                                    <i aria-hidden="true" class="fa fa-question-circle-o"></i>
                                </sup>
                            </label>
                            <?= $form->field($model, 'vendor_code', ['options'=>['class'=>'']])->textInput(['id' => 'newVendorCode', 'class'=> 'form-control form-control-own'])->label(false) ?>
                        </div>

                        <div class="form-group">
                            <label for="newPrice">
                                Price
                                <sup class="pe-help" data-toggle="tooltip" title="АLorem ipsum dolor sit amet, consectetur adipisicing elit. Sit natus beatae architecto">
                                    <i aria-hidden="true" class="fa fa-question-circle-o"></i>
                                </sup>
                            </label>
                            <?= $form->field($model, 'price', ['options'=>['class'=>'']])->textInput(['id' => 'newPrice', 'class'=> 'form-control form-control-own'])->label(false) ?>
                        </div>

                        <div class="form-group">
                            <label for="active">
                                Activate parsing
                                <sup class="pe-help" data-toggle="tooltip" title="Активирован или нет мониторинг цен для этого товара">
                                    <i aria-hidden="true" class="fa fa-question-circle-o"></i>
                                </sup>
                            </label><br>
                            <div class="btn-group" id="active">
                                <button data-for="statusField" data-value="1" class="btn btn-status-switch left<?= $model->status==1?' checked':' unchecked'; ?>" type="button"><i class="fa fa-check"></i></button>
                                <button data-for="statusField" data-value="3" class="btn btn-default btn-status-switch right" type="button"><i class="fa fa-close"></i></button>
                                <?= $form->field($model, 'status')->hiddenInput(['id'=>'statusField', 'value'=>!empty($model->status)?$model->status:1]); ?>
                            </div>
                        </div>

                    </div>
                    <div class="col-xs-6">

                        <div class="form-group">
                            <label for="exampleInputFile">Download Project image</label>
                            <span class="btn btn-default btn-file-own orange form-control-own"><?= Yii::t('app', 'Choose file'); ?></span>

                            <?= $form->field($model, 'photo')->fileInput(['id' => 'exampleInputFile', 'class' => 'form-control form-control-file file-img'])->label(false) ?>

                            <div class="help-block"></div>
                            <div data-id="<?= $model->id; ?>" class="preview-project-img">
                                <?php
                                $image = $project->getUploadsUrl($model->id, $model->photo);
                                echo $image?Html::img($image, ['class' => 'img-preview']).'<div class="close-img-button"></div>':'';
                                ?>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Create and go to the project'), ['class' => 'btn btn-own green btn-project']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                    <div class="btn-back"><a href="#" onclick="history.back()">Go Back</a></div>

                </div>
            </div>
        </div>
</section>
