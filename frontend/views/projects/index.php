<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel core\forms\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Your Projects');
$this->params['breadcrumbs'][] = $this->title;
?>

<!--begin section content-header-->
<section class="content-header content-header-own full">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb breadcrumb-own">
                <li><a href="<?= Url::to(['site/dashboard']); ?>">Dashboard</a></li>
                <li class="active">Projects</li>
            </ol>
        </div>
    </div>
</section>
<!--begin section content-header-->
<!--begin section content-->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="white-box">
                <div class="project-list-message">
                    <h4>
                        you don't have any projects. <br> Please create your first project
                    </h4>
                    <div class="btn-group-wrap">
                        <?= Html::a(Yii::t('app', 'Create Project'), ['create'], ['class' => 'btn btn-block btn-primary btn-own w-280 green']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="white-box">

                <div class="table-tools">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="btn-group-wrap">
                                <button type="button" class="btn btn-default btn-tool green" onclick="location.href='<?= Url::to(['create']); ?>'"><i class="fa fa-plus"></i>new project</button>
                                <!--<button type="button" class="btn btn-default btn-tool orange"><i class="fa fa-cloud-upload"></i>export</button>-->
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <nav aria-label="Page navigation">
                                <?php
                                echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pages,
                                    'options' => [
                                        'class' => 'pagination box-tools-navigation pagination-own',
                                    ],
                                    'prevPageLabel' => '<span><</span>',
                                    'nextPageLabel' => '<span>></span>',
                                ]);
                                ?>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => 'Showing {end} of '.$pages->totalCount.' entries',
                    'tableOptions' => ['class' => 'table table-project-list'],
                    'layout' => "{items}\n{summary}",
                    'columns' => [
                        [
                            'label'=>'ID',
                            'format'=>'raw',
                            'value'=>function($data){
                                return '#'.$data->id;
                            }
                        ],
                        [
                            'label'=>Yii::t('app', 'Project Name'),
                            'format'=>'raw',
                            'value'=>function($data){
                                return Html::a(Html::encode($data->name), ['project-links/index', 'project_id'=>$data->id], ['title'=>Html::encode($data->name)]);
                            }
                        ],
                        [
                            'label'=>Yii::t('app', 'Photo'),
                            'format'=>'raw',
                            'value'=>function($data){
                                return $data->photo?Html::img($data->getUploadsUrl($data->id, $data->photo), ['style'=>'max-width:100px; max-height:100px;']):'';
                            }
                        ],
                        [
                            'label'=>Yii::t('app', 'Vendor Code'),
                            'format'=>'raw',
                            'value'=>function($data){
                                return Html::encode($data->vendor_code);
                            }
                        ],
                        [
                            'label'=>Yii::t('app', 'Status'),
                            'format'=>'raw',
                            'value'=>function($data){
                                return $data->getStatuses($data->status);
                            }
                        ],
                        ['class' => 'yii\grid\ActionColumn',
                            'header'=>Yii::t('app', 'Action'),
                            'template' =>'{view} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<span class="fa fa-eye"></span>', ['project-links/index', 'project_id'=>$model->id], ['title'=>Yii::t('app','View')]);
                                },
                            ],
                        ],
                    ],
                ]);
                ?>
                </div>

                <div class="table-tools">
                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6">
                            <nav aria-label="Page navigation">
                                <?php
                                echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pages,
                                    'options' => [
                                        'class' => 'pagination box-tools-navigation pagination-own',
                                    ],
                                    'prevPageLabel' => '<span><</span>',
                                    'nextPageLabel' => '<span>></span>',
                                ]);
                                ?>
                            </nav>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!--begin section content-->
