<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel core\forms\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Notifications');
$this->params['breadcrumbs'][] = $this->title;

?>
<section class="content-header content-header-own full">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb breadcrumb-own">
                <li><a href="<?= Url::to(['site/dashboard']); ?>">Dashboard</a></li>
                <li class="active">Notifications</li>
            </ol>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <?php foreach ($notifications as $notification): ?>
            <div class="box box-notification collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Html::encode($notification->notification->title); ?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool orange" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool orange" data-id="<?= $notification->id; ?>" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?= Html::encode($notification->notification->message); ?>
                </div>
                <!-- /.box-body -->
            </div>
            <?php endforeach; ?>

            <div class="table-tools">
                <div class="row">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <nav aria-label="Page navigation">
                            <?php
                            echo \yii\widgets\LinkPager::widget([
                                'pagination' => $pages,
                                'options' => [
                                    'class' => 'pagination box-tools-navigation pagination-own',
                                ],
                                'prevPageLabel' => '<span><</span>',
                                'nextPageLabel' => '<span>></span>',
                            ]);
                            ?>
                        </nav>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>








