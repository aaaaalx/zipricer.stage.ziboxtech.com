<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'User profile');
$this->params['breadcrumbs'][] = $this->title;

$img = $model->icon?$model->getUploadsUrl($model->id, $model->icon):'/themes/default/img/avatar-cap.png';
?>
<section class="content-header content-header-own full">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb breadcrumb-own">
                <li><a href="<?= Url::to(['site/dashboard']); ?>">Dashboard</a></li>
                <li class="active">User Profile</li>
            </ol>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
                    <li><a href="#settings" data-toggle="tab">Settings</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="profile">
                        <div class="box-body box-profile">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="avatar-img">
                                        <img src="<?= $img; ?>" alt="">
                                    </div>
                                    <a href="<?= Url::to(['notifications/index']); ?>" class="btn btn-default btn-own orange">Notifications</a>
                                </div>
                                <div class="col-md-9">
                                    <div class="col-md-4 col-xs-6">
                                        <b><?= Yii::t('app', 'Full Name') ?></b>
                                        <p><?= $profile->username ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6">
                                        <b><?= Yii::t('app', 'Email') ?></b>
                                        <p><?= $profile->email ?></p>
                                    </div>
                                    <div class="col-md-4">
                                        <b><?= Yii::t('app', 'Your referral code') ?></b>
                                        <p><?= $profile->referral_link ?></p>
                                    </div>
                                    <div class="col-xs-12">
                                        <b><?= Yii::t('app', 'Your referral link') ?></b>
                                        <p><?= Yii::$app->params['frontendHost'] ?>site/signup?code=<?= $profile->referral_link ?></p>
                                    </div>
                                    <div class="col-xs-12">
                                        <ul class="list-group list-group-own list-group-unbordered">
                                            <?php
                                            $links = $profile->getLinks();
                                            $linksCount = $links['linksCount'];
                                            $projectsCount = $links['projectsCount'];
                                            ?>
                                            <li class="list-group-item">
                                                <b><?= Yii::t('app', 'Projects') ?></b> <a class="pull-right"><?= $projectsCount ?></a>
                                            </li>
                                            <li class="list-group-item">
                                                <b><?= Yii::t('app', 'Links') ?></b> <a class="pull-right"><?= $linksCount ?></a>
                                            </li>
                                            <!--
                                            <li class="list-group-item">
                                                <b>Your current tarif</b> <a class="pull-right">Basic</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Valid till</b> <a class="pull-right">25.10.2018</a>
                                            </li>
                                            -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="settings">
                        <div class="box-body">

                            <div class="profile-form">

                                <?php $form = ActiveForm::begin(['options'=>['class' => "form-profile form-dark"]]); ?>

                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1">
                                        <div class="form-group">
                                            <div class="preview-avatar-img">
                                                <img src="<?= $img; ?>" alt="">
                                            </div>
                                            <div class="btn btn-default btn-file-own green form-control-own">
                                                <?= $form->field($model, 'icon')->fileInput(['id' => 'exampleInputFile', 'class' => 'form-control form-control-file file-avatar'])->label(Yii::t('app', 'Upload Avatar')) ?>
                                            </div>
                                        </div>

                                <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'placeholder' => $model->attributeLabels()['username'], 'class' => 'form-control form-control-own'])->label(Yii::t('app', 'Name')) ?>

                                <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => $model->attributeLabels()['email'], 'class' => 'form-control form-control-own']) ?>

                                <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Password'), 'class' => 'form-control form-control-own']) ?>

                                <?= $form->field($model, 'password_repeat')->passwordInput(['placeholder' => Yii::t('app', 'Confirm password'), 'class' => 'form-control form-control-own'])->label(Yii::t('app', 'Confirm Password')) ?>

                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-danger btn-own green']) ?>
                                        </div>
                                    </div>
                                </div>

                                <?php ActiveForm::end(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

