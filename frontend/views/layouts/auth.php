<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);

$page = basename(Yii::$app->request->pathInfo);

$classes = [
    'login' => 'hold-transition login-page login-page-own',
    'signup' => 'hold-transition register-page register-page-own'
];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <link rel="shortcut icon" href="#" type="image/x-icon" sizes="32x32">
    <link rel="shortcut icon" href="#" type="image/x-icon" sizes="64x64">

</head>
<body class="<?= $classes[$page]; ?>">
<?php $this->beginBody() ?>

<?= Alert::widget() ?>
<?= $content ?>

<?php $this->endBody() ?>

</body>

</html>
<?php $this->endPage() ?>