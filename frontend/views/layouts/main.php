<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use core\entity\NotificationsUsers;
use common\helpers\MainHelper;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="#" type="image/x-icon" sizes="32x32">
    <link rel="shortcut icon" href="#" type="image/x-icon" sizes="64x64">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body class="sidebar-mini fixed">
<?php $this->beginBody() ?>
<!--begin wrappwr-->
<div class="wrapper">
    <!-- Main Header -->
    <header class="main-header main-header-own">
        <!-- Logo -->
        <a href="/" class="logo logo-own">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img src="/themes/default/img/logo-zibox-mini.png" alt=""></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg logo-lg-own"><img src="/themes/default/img/logo-zibox.png" alt=""></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top navbar-main-own" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle sidebar-toggle-own" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">

                <ul class="nav navbar-nav navbar-userinfo">

                    <!-- Notifications Menu -->
                    <li class="dropdown notifications-menu notifications-menu-own">
                        <!-- Menu toggle button -->
                        <?php
                            $notifications = NotificationsUsers::getUserNotifications(5);
                            $notificationsActiveCount = NotificationsUsers::getActiveNotificationsCount();
                            $notificationsCount = NotificationsUsers::getNotificationsCount();
                        ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-counter green"><?= $notificationsActiveCount; ?></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-own">
                            <li class="header header-own">You have <?= $notificationsCount; ?> notifications</li>
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    <?php foreach ($notifications as $notification): ?>
                                    <li>
                                        <!-- start notification -->
                                        <i class="fa fa-users text-aqua"></i><?= MainHelper::cutString(Html::encode($notification->notification->title), 50); ?>
                                        <div><?= MainHelper::cutString(Html::encode($notification->notification->message), 50); ?><hr></div>
                                    </li>
                                    <!-- end notification -->
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                            <li class="footer footer-own"><a href="<?= Url::to(['notifications/index']); ?>">View all</a></li>
                        </ul>
                    </li>

                    <!-- User Account Menu -->
                    <li class="user user-menu user-menu-own">
                        <!-- The user image in the navbar-->
                        <?php $userIcon = Yii::$app->user->identity->icon; ?>
                        <img src="<?= $userIcon?Yii::$app->user->identity->getUploadsUrl(Yii::$app->user->identity->id, Yii::$app->user->identity->icon):'/themes/default/img/avatar-cap.png'; ?>" class="user-image user-image-own" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs username"><?= Yii::$app->user->identity->username; ?></span>
                        </a>
                    </li>

                    <!-- Control Sidebar Toggle Button -->
                    <li class="dropdown user-settings user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gears"></i></a>
                        <ul class="dropdown-menu dropdown-menu-own ">
                            <!-- The user image in the menu -->
                            <li class="user-header ">
                                <img src="/themes/default/img/avatar-cap.png" class="img-circle" alt="User Image">
                                <p>
                                    <?= Yii::$app->user->identity->username; ?>
                                    <small>Member since Nov. <?= date('Y', Yii::$app->user->identity->created_at); ?></small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?= Url::to(['profile/index']); ?>" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?= Url::to(['site/logout']); ?>" data-method="POST" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar main-sidebar-own">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- search form (Optional) -->
            <!--
            <form action="#" method="get" class="sidebar-form form-orange navbar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control form-control-own" placeholder="Search...">
                    <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat search-btn-own"><i class="fa fa-search fa-search-own"></i>
            </button>
          </span>
                </div>
            </form>
            -->
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu sidebar-menu-own" data-widget="tree">
                <!-- Optionally, you can add icons to the links -->
                <li class="active">
                    <a href="<?= Url::to(['site/dashboard']); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
                </li>
                <li>
                    <a href="<?= Url::to(['profile/index']); ?>"><i class="fa fa-user"></i> <span>Profile</span></a>
                </li>
                <li>
                    <a href="<?= Url::to(['projects/index']); ?>"><i class="fa fa-pencil-square-o"></i> <span>Projects</span></a>
                </li>
                <!--<li><a href="finance.html"><i class="fa fa-shopping-cart"></i> <span>Finance</span></a></li>-->
                <!--<li><a href="tarifs-page.html"><i class="fa fa-dollar"></i> <span>Tarifs</span> </a></li>-->
                <li>
                    <a href="<?= Url::to(['site/logout']); ?>" data-method="POST"><i class="fa fa-sign-out"></i> <span>Log out</span> </a>
                </li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
    <!--end aside-->

    <!--begin content-wrapper-->
    <div class="content-wrapper content-wrapper-own">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
    <!--end content-wrapper-->

    <!--begin footer-->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; <?= date('Y') ?> <a href="http://ziboxtech.com/">ZiBox Tech</a>.</strong> All rights
        reserved.
    </footer>
    <!--end footer-->

</div>
<!--end wrapper-->

<!--SCRIPTS-->
<?php $this->endBody() ?>

</body>

</html>
<?php $this->endPage() ?>