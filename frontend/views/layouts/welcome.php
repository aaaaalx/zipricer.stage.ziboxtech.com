<?php
//сделать консольный контроллер для крона и с возможностью парсить по одной ссылке 3ч
//сделать нормальное удаление для профилей, проектов, ссылок проектов и ссылок, нотификаций 2ч
//сделать кнопку парсинга в админке и возможность парсить по одной ссылке (возможно сразу несколько) 1д
//проверить все ссылки 1ч
//разобраться с редактированием ссылок 2ч
//разобраться со статусами ссылок 1ч
//сделать добавление ссылок которые не поддерживаются (при добавлении ссылок) и отсылание нотификаций админу при этом 3ч
//проверить работу нотификаций 1ч

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

	<link rel="shortcut icon" href="#" type="image/x-icon" sizes="32x32">
	<link rel="shortcut icon" href="#" type="image/x-icon" sizes="64x64">

	<link rel="stylesheet" type="text/css" href="/themes/main/css/libs.css">
	<link rel="stylesheet" type="text/css" href="/themes/main/css/style.css">

</head>

<body>
<?php $this->beginBody() ?>
	<!--begin header-->
	<header class="fixed">
		<!--begin menu-->
		<div class="container width-100">
			<div class="row">
				<div class="col-xs-3">
					<div class="logo-header">
						<a href="/"><img src="/themes/main/img/logo.png" alt=""></a>
					</div>
				</div>
				<div class="col-xs-9">
					<button class="button-menu-toggle"></button>
					<div class="login-group">
						<div class="login-box">
                            <?php if(Yii::$app->user->isGuest): ?>
							<a href="<?= Url::to(['site/login']); ?>">Вход</a>
                            <?php else: ?>
                                <?= Html::a('Выход', ['site/logout'], ['data-method'=>'post']); ?>
                            <?php endif; ?>
						</div>
                        <?php if(Yii::$app->user->isGuest): ?>
						<div class="registration-box">
							<a href="<?= Url::to(['site/signup']); ?>">Регистрация</a>
						</div>
                        <?php else: ?>
                        <div class="registration-box">
                            <a href="<?= Url::to(['site/dashboard']); ?>">Dashboard</a>
                        </div>
                        <?php endif; ?>
					</div>
					<div class="phone-box">
						<ul class="phone-list">
							<li class="dropdown dropdown-phone">
								<a href="#" class="dropdown-toggle dropdown-phone-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">+38 (044) 587-78-28<span class="caret caret-own  caret-open"></span></a>
								<ul class="dropdown-menu dropdown-phone-menu">
									<li><a href="tel:+380445877828">+38 (044) 587-78-28</a></li>
									<li><a href="tel:+380948877828">+38 (094) 887-78-28</a></li>
									<li><a href="tel:+380738877828">+38 (073) 887-78-28</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="email-box">
						<a href="mailto:ua@ziboxtech.com">ua@ziboxtech.com</a>
					</div>
				</div>

				<div class="menu-box">
					<div class="container width-100">
						<div class="row relative">
							<div class="menu-list">
								<ul>
									<li><a href="#">о нас</a></li>
									<li><a href="#">тарифы и цены</a></li>
									<li><a href="#">отзывы</a></li>
									<li><a href="#">контакты</a></li>
								</ul>
							</div>
							<button class="button-menu-toggle green-button"></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--end menu-->
	</header>
	<!--end header-->

    <?= Alert::widget() ?>
    <?= $content ?>

    <!--begin footer-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer-links-list">
                        <ul>
                            <li><a href="#">О приложении</a></li>
                            <li><a href="#">Тарифные планы</a></li>
                            <li><a href="#">Отзывы</a></li>
                            <li><a href="#">Регистрация</a></li>
                            <li><a href="#">Обратная связь</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="divider"></div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="copyright">
                        Copyright <?= date('Y') ?> All rights reserved. Created by <a href="http://ziboxtech.com/">ZiBox Tech</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--end footer-->

    <!--begin modal form-->
    <!-- Modal -->
    <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="modalFormLabel">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-own">
                <div class="modal-header modal-header-own">

                </div>
                <div class="modal-body modal-body-own">
                    <div class="form-wrap">
                        <div class="form-header">
                            <h4>Оставить заявку</h4>
                        </div>
                        <div class="form-description">
                            Оставьте ваши контактные данные, специалист свяжется с вами в ближайшее время
                        </div>
                        <form action="#" class="form form-black form-popup">
                            <div class="form-group form-group-own has-error">
                                <input type="text" class="form-control form-control-own form-name" id="formPopupName" placeholder="Name*" required>
                                <span id="helpBlock1" class="help-block help-block-own">Please enter your name</span>
                                <span class="input-icon-error"></span>
                            </div>
                            <div class="form-group form-group-own has-success">
                                <input type="text" class="form-control form-control-own" id="formPopupPhone" placeholder="Phone" required>
                                <span id="helpBlock2" class="help-block help-block-own">Please enter your phone number</span>
                                <span class="input-icon-success"></span>
                            </div>
                            <div class="form-group form-group-own">
                                <input type="email" class="form-control form-control-own" id="formPopupEmail" placeholder="Email" required>
                                <span id="helpBlock3" class="help-block help-block-own">Please enter your email</span>
                            </div>
                            <button type="submit" class="btn btn-default btn-submit main-button orange-button">Отправить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end modal form-->
    <div class="btn-form-call" data-toggle="modal" data-target="#modalForm"><div>есть вопросы?</div></div>


    <!--SCRIPTS-->
    <?php $this->endBody() ?>
    <script type="text/javascript" src="/themes/main/libs/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="/themes/main/libs/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/themes/main/libs/js/slick.js"></script>
    <script type="text/javascript" src="/themes/main/libs/js/slick-lightbox.js"></script>
    <script type="text/javascript" src="/themes/main/libs/js/wow.min.js"></script>
    <script> new WOW().init(); </script>
    <script type="text/javascript" src="/themes/main/libs/js/jquery.maskedinput.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="/themes/main/js/main.js"></script>
</body>

</html>
<?php $this->endPage() ?>