<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Register a new membership';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="register-box register-box-own">
    <div class="register-logo register-logo-own">
        <a href="#"><img src="/themes/default/img/ZiBox_logo_contour.svg" alt=""></a>
    </div>
    <div class="register-box-body register-box-body-own">
        <p class="login-box-msg login-box-msg-own"><?= Html::encode($this->title) ?></p>


        <?php $form = ActiveForm::begin(['id' => 'form-signup', 'class'=>'form-white']); ?>

            <?= $form->field($model, 'username')->textInput(['class' => 'form-control form-control-own', 'placeholder'=>Yii::t('app','Full name')])->label(false) ?>

            <?= $form->field($model, 'email')->textInput(['class' => 'form-control form-control-own', 'placeholder'=>Yii::t('app','Email')])->label(false) ?>

            <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control form-control-own', 'placeholder'=>Yii::t('app','Password')])->label(false) ?>

            <?= $form->field($model, 'password_repeat')->passwordInput(['class' => 'form-control form-control-own', 'placeholder'=>Yii::t('app','Retype password')])->label(false) ?>

            <div class="row">
                <!-- /.col -->
                <div class="col-xs-12">
                    <?= Html::submitButton('Register', ['class' => 'btn btn-primary btn-block btn-own white', 'name' => 'signup-button']) ?>
                </div>
                <!-- /.col -->
            </div>

        <?php ActiveForm::end(); ?>

        <?= Html::a('I already have a membership', ['site/login'], ['class'=>'text-center white-text']) ?>
    </div>
</div>
