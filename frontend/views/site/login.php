<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Sign in to start your session';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-box login-box-own">
    <div class="login-logo login-logo-own">
        <a href="/"><img src="/themes/default/img/ZiBox_logo_contour.svg" alt=""></a>
    </div>

    <div class="login-box-body login-box-body-own">
        <p class="login-box-msg login-box-msg-own"><?= Html::encode($this->title) ?></p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'class'=>'form-white']); ?>

            <?= $form->field($model, 'email')->textInput(['class'=>'form-control form-control-own', 'placeholder'=>'Email'])->label(false) ?>
            <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control form-control-own', 'placeholder'=>'Password'])->label(false) ?>

            <div class="row">
                <!-- /.col -->
                <div class="col-xs-12">
                    <?= Html::submitButton('Sign In', ['class' => 'btn btn-primary btn-block btn-own white', 'name' => 'login-button']) ?>
                </div>
                <!-- /.col -->

            </div>
        <?php ActiveForm::end(); ?>

        <?= Html::a('forgot my password', ['site/request-password-reset'], ['class'=>'blue-text']) ?><br>
        <?= Html::a('Register a new membership', ['site/signup'], ['class'=>'text-center blue-text']) ?>

    </div>
</div>