<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<div class="login-box login-box-own">
    <div class="login-logo login-logo-own">
        <a href="/"><img src="/themes/default/img/ZiBox_logo_contour.svg" alt=""></a>
    </div>

    <div class="login-box-body login-box-body-own">
        <p class="login-box-msg login-box-msg-own"><?= Html::encode($this->title) ?></p>

        <div class="row">
            <!-- /.col -->
            <div class="col-xs-12">
                <?= nl2br(Html::encode($message)) ?>
            </div>
            <!-- /.col -->

        </div>

    </div>
</div>