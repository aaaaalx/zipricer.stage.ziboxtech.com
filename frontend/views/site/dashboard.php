<?php
use common\helpers\MainHelper;
use common\helpers\ProjectLinksHelper;

?>
<section class="content-header content-header-own full">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h1>Dashboard</h1>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb breadcrumb-own">
                <li><a href="/">Main</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-lg-3 col-sm-6 grey-brd-resp">
                        <ul class="col-in">
                            <li>
                                <span class="circle green-bg">
                                  <i class="zi-icon zi-instock fa fa-check"></i>
                                </span>
                            </li>
                            <li class="col-middle">
                                <h4>Projects Links</h4>
                                <?php
                                $projectsCount = $profile->getProjectsCount();
                                ?>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-green progress-bar-xs green-bg" role="progressbar" aria-valuenow="<?= MainHelper::valBetween($projectsCount, 0, 100); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= MainHelper::valBetween($projectsCount, 0, 100); ?>%">
                                        <span class="sr-only"><?= MainHelper::valBetween($projectsCount, 0, 100); ?>% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="box-counter"><?= $projectsCount; ?></span>
                            </li>
                        </ul>

                    </div>

                    <div class="col-lg-3 col-sm-6 grey-brd-resp">
                        <ul class="col-in">
                            <li>
                                <span class="circle blue-bg">
                                  <i class="zi-icon zi-instock fa fa-battery-half"></i>
                                </span>
                            </li>
                            <li class="col-middle">
                                <h4>Total links</h4>
                                <?php
                                $projectsLinksCount = $profile->getUserLinksCount();
                                ?>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-green progress-bar-xs blue-bg" role="progressbar" aria-valuenow="<?= MainHelper::valBetween($projectsLinksCount, 0, 100); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= MainHelper::valBetween($projectsLinksCount, 0, 100); ?>%">
                                        <span class="sr-only"><?= MainHelper::valBetween($projectsLinksCount, 0, 100); ?>% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="box-counter"><?= $projectsLinksCount; ?></span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-sm-6 grey-brd-resp">
                        <ul class="col-in">
                            <li>
                                <span class="circle red-bg">
                                  <i class="zi-icon zi-instock fa fa-close"></i>
                                </span>
                            </li>
                            <li class="col-middle">
                                <h4>Total Active links</h4>
                                <?php
                                $projectsActiveLinksCount = $profile->getActiveLinksCount();
                                ?>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-green progress-bar-xs red-bg" role="progressbar" aria-valuenow="<?= MainHelper::valBetween($projectsActiveLinksCount, 0, 100); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= MainHelper::valBetween($projectsActiveLinksCount, 0, 100); ?>%">
                                        <span class="sr-only"><?= MainHelper::valBetween($projectsActiveLinksCount, 0, 100); ?>% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="box-counter"><?= $projectsActiveLinksCount; ?></span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <ul class="col-in">
                            <li>
                                <span class="circle orange-bg">
                                  <i class="zi-icon zi-instock fa fa-spinner"></i>
                                </span>
                            </li>
                            <li class="col-middle">
                                <h4>Total Inactive links</h4>
                                <?php
                                $projectsNotActiveLinksCount = $profile->getNotActiveLinksCount();
                                ?>
                                <div
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-green progress-bar-xs orange-bg" role="progressbar" aria-valuenow="<?= MainHelper::valBetween($projectsNotActiveLinksCount, 0, 100); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= MainHelper::valBetween($projectsNotActiveLinksCount, 0, 100); ?>%">
                                        <span class="sr-only"><?= MainHelper::valBetween($projectsNotActiveLinksCount, 0, 100); ?>% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="box-counter"><?= $projectsNotActiveLinksCount; ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <?php
        $randLinks = $profile->getRandLinks(2);
        $scripts = [];
        foreach($randLinks as $key => $link):
            isset($scripts[$key+1])?:$scripts[$key+1]=[];
            $linkData = (array)$link->history;
            $linkInfo = $linkData[0]??[];
            $scripts[$key+1] = array_map([ProjectLinksHelper::class, 'chartConfig'], $linkData);
        ?>
        <div class="col-lg-6">
            <div class="box box-info box-blue">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= @$linkInfo['link_name']; ?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body chart-responsive">
                    <div class="chart" id="line-chart-<?= $key+1; ?>"></div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>

        <script>
            var chartsData = <?= json_encode($scripts, true); ?>;
        </script>

    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-lg-3 col-sm-6 grey-brd-resp">
                        <ul class="col-in">
                            <li>
                    <span class="circle green-bg">
                      <i class="zi-icon zi-instock fa fa-check"></i>
                    </span>
                            </li>
                            <li class="col-middle">
                                <h4>Higher price links</h4>
                                <?php
                                $projectsHigherPriceLinksCount = $profile->getHigherPriceLinksCount();
                                ?>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-green progress-bar-xs green-bg" role="progressbar" aria-valuenow="<?= MainHelper::valBetween($projectsHigherPriceLinksCount, 0, 100); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= MainHelper::valBetween($projectsHigherPriceLinksCount, 0, 100); ?>%">
                                        <span class="sr-only"><?= MainHelper::valBetween($projectsHigherPriceLinksCount, 0, 100); ?>% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="box-counter"><?= $projectsHigherPriceLinksCount; ?></span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-sm-6 grey-brd-resp">
                        <ul class="col-in">
                            <li>
                    <span class="circle blue-bg">
                      <i class="zi-icon zi-instock fa fa-battery-half"></i>
                    </span>
                            </li>
                            <li class="col-middle">
                                <h4>Lower price links</h4>
                                <?php
                                $projectsLowerPriceLinksCount = $profile->getLowerPriceLinksCount();
                                ?>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-green progress-bar-xs blue-bg" role="progressbar" aria-valuenow="<?= MainHelper::valBetween($projectsLowerPriceLinksCount, 0, 100); ?>" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                        <span class="sr-only"><?= MainHelper::valBetween($projectsLowerPriceLinksCount, 0, 100); ?>% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="box-counter"><?= $projectsLowerPriceLinksCount; ?></span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-sm-6 grey-brd-resp">
                        <ul class="col-in">
                            <li>
                    <span class="circle red-bg">
                      <i class="zi-icon zi-instock fa fa-close"></i>
                    </span>
                            </li>
                            <li class="col-middle">
                                <h4>Same price links</h4>
                                <?php
                                $projectsSamePriceLinksCount = $profile->getLowerPriceLinksCount();
                                ?>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-green progress-bar-xs red-bg" role="progressbar" aria-valuenow="<?= MainHelper::valBetween($projectsSamePriceLinksCount, 0, 100); ?>" aria-valuemin="0" aria-valuemax="100" style="width: 5%">
                                        <span class="sr-only"><?= MainHelper::valBetween($projectsSamePriceLinksCount, 0, 100); ?>% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="box-counter"><?= $projectsSamePriceLinksCount; ?></span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <ul class="col-in">
                            <li>
                    <span class="circle orange-bg">
                      <i class="zi-icon zi-instock fa fa-spinner"></i>
                    </span>
                            </li>
                            <li class="col-middle">
                                <h4>No available links</h4>
                                <?php
                                $notAvailablePriceLinksCount = $profile->getNotAvailablePriceLinksCount();
                                ?>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-green progress-bar-xs orange-bg" role="progressbar" aria-valuenow="<?= MainHelper::valBetween($notAvailablePriceLinksCount, 0, 100); ?>" aria-valuemin="0" aria-valuemax="100" style="width: 5%">
                                        <span class="sr-only"><?= MainHelper::valBetween($notAvailablePriceLinksCount, 0, 100); ?>% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="box-counter"><?= $notAvailablePriceLinksCount; ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>