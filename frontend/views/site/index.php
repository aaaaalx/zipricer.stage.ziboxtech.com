<?php

/* @var $this yii\web\View */

$this->title = 'ZiPricer';
?>
<!--begin section-intro-->
<section>
    <div class="section-intro-slider section-light">
        <div class="slider-main-intro">
            <div class="item">
                <img src="/themes/main/img/collage_2200x931.jpg" alt="">
                <div class="slide-description">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-xs-12">
                                <div class="inner-container">
                                    <div class="intor-slogan wow fadeIn">
                                        <span>zibox technologies</span>
                                    </div>
                                    <div class="intro-title wow fadeIn">
                                        <h2>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="/themes/main/img/collage_2200x931_v1-2.jpg" alt="">
                <div class="slide-description">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-xs-12">
                                <div class="inner-container">
                                    <div class="intro-slogan">
                                        <span>zibox technologies</span>
                                    </div>
                                    <div class="intro-title">
                                        <h2>Lorem ipsum dolor sit amet</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="/themes/main/img/collage_2200x931_v1-3.jpg" alt="">
                <div class="slide-description">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-xs-12">
                                <div class="inner-container">
                                    <div class="intro-slogan">
                                        <span>zibox technologies</span>
                                    </div>
                                    <div class="intro-title">
                                        <h2>Lorem ipsum dolor sit amet</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section intro-->

<!--begin section-->
<section>
    <div class="section-base-pt section-dark">
        <div class="section-inner-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12 wow fadeInLeft">
                        <div class="section-title-level-2 ">
                            <h2>О приложении</h2>
                        </div>
                        <div class="section-description ">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore ab placeat nihil, alias culpa, autem? Qui unde ut, delectus beatae assumenda esse labore. Dolor vero libero delectus id esse quod.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12 wow fadeInRight">
                        <div class="button-right">
                            <a href="#" class="main-button orange-button">Посмотреть тарифы</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!---end section-->

<!--begin section-->
<section>
    <div class="section-base section-list-item section-dark">
        <div class="container">
            <div class="section-title-level-2 title-center wow fadeInUp">
                <h2>Почему выбирают нас?</h2>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.5s">
                    <div class="thumbnail thumbnail-own">
                        <div class="thumbnail-img-own">
                            <img src="/themes/main/img/icons/thumb-icon.png" alt="">
                        </div>
                        <div class="caption caption-thumbnail">
                            <div class="thumbnail-title">
                                <h3>Lorem ipsum dolor</h3>
                            </div>
                            <div class="thumbnail-description">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor fuga nesciunt aliquam tenetur
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.7s">
                    <div class="thumbnail thumbnail-own">
                        <div class="thumbnail-img-own">
                            <img src="/themes/main/img/icons/thumb-icon.png" alt="">
                        </div>
                        <div class="caption caption-thumbnail">
                            <div class="thumbnail-title">
                                <h3>Lorem ipsum dolor</h3>
                            </div>
                            <div class="thumbnail-description">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor fuga nesciunt aliquam tenetur
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.9s">
                    <div class="thumbnail thumbnail-own">
                        <div class="thumbnail-img-own">
                            <img src="/themes/main/img/icons/thumb-icon.png" alt="">
                        </div>
                        <div class="caption caption-thumbnail">
                            <div class="thumbnail-title">
                                <h3>Lorem ipsum dolor</h3>
                            </div>
                            <div class="thumbnail-description">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor fuga nesciunt aliquam tenetur
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="1.1s">
                    <div class="thumbnail thumbnail-own">
                        <div class="thumbnail-img-own">
                            <img src="/themes/main/img/icons/thumb-icon.png" alt="">
                        </div>
                        <div class="caption caption-thumbnail">
                            <div class="thumbnail-title">
                                <h3>Lorem ipsum dolor</h3>
                            </div>
                            <div class="thumbnail-description">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor fuga nesciunt aliquam tenetur
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-->

<!--begin section-offer-->
<section>
    <div class="section-base section-offer green section-light">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="offer-text-box wow fadeInLeft"><b>Заинтересовались?</b> Зарегистрируйтесь и начните пользоваться приложением прямо сейчас!</div>
                    <div class="button-box wow fadeInRight">
                        <a href="#" class="main-button orange-button w-280">Начать прямо сейчас</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-offer-->
<!--begin section-slider-->
<section>
    <div class="section-base section-slider section-center text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 col-xs-12 col-xs-offset-0 wow zoomIn">
                    <div class="section-title-level-2">
                        <h2>Lorem ipsum dolor sit amet</h2>
                    </div>
                    <div class="section-description">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus cupiditate enim aliquam. Provident mollitia culpa voluptatem, porro aspernatur aliquam cupiditate quidem. Temporibus esse blanditiis, facilis sapiente accusamus nam similique, et.</p>
                    </div>
                </div>
                <div class="col-xs-12 wow zoomIn">
                    <div class="slider-screenshots">
                        <a href="/themes/main/img/screenshot.png">
                            <div class="item">
                                <div class="img-box">
                                    <img src="/themes/main/img/screenshot.png" height="414" width="660" alt="">
                                </div>
                            </div>
                        </a>
                        <a href="/themes/main/img/screenshot.png">
                            <div class="item">
                                <div class="img-box">
                                    <img src="/themes/main/img/screenshot.png" height="414" width="660" alt="">
                                </div>
                            </div>
                        </a>
                        <a href="/themes/main/img/screenshot.png">
                            <div class="item">
                                <div class="img-box">
                                    <img src="/themes/main/img/screenshot.png" height="414" width="660" alt="">
                                </div>
                            </div>
                        </a>
                        <a href="/themes/main/img/screenshot.png">
                            <div class="item">
                                <div class="img-box">
                                    <img src="/themes/main/img/screenshot.png" height="414" width="660" alt="">
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-slider-->
<!--begin section-tarifs-->
<section>
    <div class="section-base section-dark section-tarifs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 wow fadeInUp">
                    <div class="section-title-level-2 text-center">
                        <h2>Тарифные планы</h2>
                    </div>
                </div>
                <div class="col-xs-12 wow fadeInUp">
                    <div class="slider-tarifs">
                        <div class="item">
                            <div class="item-tarif-wrap green">
                                <div class="item-tarif-header">
                                    <h4>Premium</h4>
                                </div>
                                <div class="item-tarif-price">
                                    <span>$45</span>
                                    <span>per month</span>
                                </div>
                                <div class="item-tarif-list">
                                    <ul>
                                        <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit.  </li>
                                        <li>In aut ipsa doloremque architecto aliquid placeat fuga dignissimos dolore!</li>
                                        <li>Minima magnam ab ducimus aspernatur porro provident recusandae esse neque non sunt.</li>
                                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </li>
                                        <li>Minima magnam ab ducimus aspernatur porro provident recusandae esse neque non sunt.</li>
                                    </ul>
                                </div>
                                <div class="tarif-label-green">recommended</div>
                                <button type="button" class="btn main-button green-button w-280" data-toggle="modal" data-target="#modalForm">Купить</button>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-tarif-wrap blue">
                                <div class="item-tarif-header">
                                    <h4>Professional</h4>
                                </div>
                                <div class="item-tarif-price">
                                    <span>$45</span>
                                    <span>per month</span>
                                </div>
                                <div class="item-tarif-list">
                                    <ul>
                                        <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit.  </li>
                                        <li>In aut ipsa doloremque architecto aliquid placeat fuga dignissimos dolore!</li>
                                        <li>Minima magnam ab ducimus aspernatur porro provident recusandae esse neque non sunt.</li>
                                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </li>
                                        <li>Minima magnam ab ducimus aspernatur porro provident recusandae esse neque non sunt.</li>
                                    </ul>
                                </div>
                                <button type="button" class="btn main-button blue-button w-280" data-toggle="modal" data-target="#modalForm">Купить</button>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-tarif-wrap blue">
                                <div class="item-tarif-header">
                                    <h4>Professional 2</h4>
                                </div>
                                <div class="item-tarif-price">
                                    <span>$45</span>
                                    <span>per month</span>
                                </div>
                                <div class="item-tarif-list">
                                    <ul>
                                        <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit.  </li>
                                        <li>In aut ipsa doloremque architecto aliquid placeat fuga dignissimos dolore!</li>
                                        <li>Minima magnam ab ducimus aspernatur porro provident recusandae esse neque non sunt.</li>
                                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </li>
                                        <li>Minima magnam ab ducimus aspernatur porro provident recusandae esse neque non sunt.</li>
                                    </ul>
                                </div>
                                <button type="button" class="btn main-button blue-button w-280" data-toggle="modal" data-target="#modalForm">Купить</button>>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-tarif-wrap blue">
                                <div class="item-tarif-header">
                                    <h4>Professional 2</h4>
                                </div>
                                <div class="item-tarif-price">
                                    <span>$45</span>
                                    <span>per month</span>
                                </div>
                                <div class="item-tarif-list">
                                    <ul>
                                        <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit.  </li>
                                        <li>In aut ipsa doloremque architecto aliquid placeat fuga dignissimos dolore!</li>
                                        <li>Minima magnam ab ducimus aspernatur porro provident recusandae esse neque non sunt.</li>
                                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </li>
                                        <li>Minima magnam ab ducimus aspernatur porro provident recusandae esse neque non sunt.</li>
                                    </ul>
                                </div>
                                <button type="button" class="btn main-button blue-button w-280" data-toggle="modal" data-target="#modalForm">Купить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-tarifs-->
<!--begin section-testimonials-->
<section>
    <div class="section-base section-testimonials">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-xs-12 wow fadeInLeft">
                    <div class="section-title-level-2">
                        <h2>Отзывы наших пользователей</h2>
                    </div>
                    <div class="slider-testimonials">
                        <div class="item">
                            <div class="item-wrap-testimonial">
                                <div class="username">Long First and Second Name</div>
                                <div class="testimonial-text">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, nobis! Eveniet ratione accusantium, facere perspiciatis architecto nam aliquid a culpa fugit numquam assumenda similique doloremque, rerum totam iure incidunt sequi.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-wrap-testimonial">
                                <div class="username">Long First and Second Name</div>
                                <div class="testimonial-text">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, nobis! Eveniet ratione accusantium, facere perspiciatis architecto nam aliquid a culpa fugit numquam assumenda similique doloremque, rerum totam iure incidunt sequi.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-wrap-testimonial">
                                <div class="username">Long First and Second Name</div>
                                <div class="testimonial-text">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, nobis! Eveniet ratione accusantium, facere perspiciatis architecto nam aliquid a culpa fugit numquam assumenda similique doloremque, rerum totam iure incidunt sequi.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-xs-12 wow fadeInRight">
                    <div class="img-box">
                        <img src="/themes/main/img/testimonial-img.png" height="270" width="405" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end sectioon-testimonials-->
<!--begin section-offer-->
<section>
    <div class="section-base section-offer green section-light">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="offer-text-box wow fadeInLeft"><b>Заинтересовались?</b> Зарегистрируйтесь и начните пользоваться приложением прямо сейчас!</div>
                    <div class="button-box wow fadeInRight">
                        <a href="#" class="main-button orange-button w-280">Начать прямо сейчас</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-offer-->
