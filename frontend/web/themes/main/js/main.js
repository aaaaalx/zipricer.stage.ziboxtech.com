
/*header menu*/
$(".button-menu-toggle").click(function(){
	$(".menu-box").toggleClass("show");
	$(this).toggleClass("green-button");    
});

/*responsiveNav*/
$(document).ready(function(){
	function responsiveNav(){
		var windowWidth = window.innerWidth;
		if (windowWidth > 768){
			$(".login-group").appendTo("header .col-xs-9");

		}
		else {

			$(".login-group").appendTo(".menu-list");
		};
	};
	responsiveNav();
	$(window).resize(function(){
		responsiveNav();
	});
});

/*collapse sidebar*/

$(".left-panel-toggle").click(function(){
	$(".dashboard-page").toggleClass("is-collapsed");
});

/*phone-caret*/
$(".dropdown-phone-toggle").click(function(){
	$(".caret-own").toggleClass("caret-open").toggleClass("caret-close");
});

/*dropdown sidebar-link*/
$(".sidebar-link.dropdown-toggle").click(function(){
	$(this).next(".dropdown-nav").slideToggle();
	$(this).find(".title").toggleClass("open-drop").toggleClass("close-drop");
});

/*button form call*/
var btnFormCall = $(".btn-form-call");
btnFormCall.click(function(){
	$(this).fadeOut();
});

$(window).scroll(function() {
	var amountScrolled = 200;
	if ( $(window).scrollTop() > amountScrolled ) {
		btnFormCall.fadeIn('slow');
	} else {
		btnFormCall.fadeOut('slow');
	}
}); 

 

/*slider-intro*/
$(".slider-main-intro").slick({
	slidesToShow: 1,
	infifte: true,
	arrows: true,
	nextArrow: '<div class="arrow-next"></div>',
	prevArrow: '<div class="arrow-prev"></div>',
});

/*slider-screenshots*/
$(".slider-screenshots").slick({
	slidesToShow: 3,	
	slidesToScroll: 2,
	infinite: true, 
	arrows: true,
	dots: true,
	nextArrow: '<div class="arrow-next"></div>',
	prevArrow: '<div class="arrow-prev"></div>',
	responsive: [
	{
		breakpoint: 1200,
		settings: {
			slidesToShow: 2,
			slidesToScroll: 1,

		}
	},
	{
		breakpoint: 800,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
		}
	}  
	]
});
$(".slider-screenshots").each(function(){
	$(this).slickLightbox();
});
/*slider-tarifs*/
$(".slider-tarifs").slick({
	centerMode: true,  
	slidesToShow: 1,
	centerPadding: '33.3333%',  
	arrows: false,
	dots: true,
	responsive: [
	{
		breakpoint: 1200,
		settings: {
			slidesToShow: 2,
			slidesToScroll: 1,
			centerMode: false,
			centerPadding: '0',
			arrows: true,
			nextArrow: '<div class="arrow-next"></div>',
			prevArrow: '<div class="arrow-prev"></div>',

		}
	},
	{
		breakpoint: 992,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			centerMode: false,
			centerPadding: '0',
			arrows: true,
			nextArrow: '<div class="arrow-next"></div>',
			prevArrow: '<div class="arrow-prev"></div>',
		}
	}  
	]
});
/*autoHeight slider tarifs items*/
function itemTarifAutoheight(){
	var minHeight = 0,
	itemTarif = $(".slider-tarifs .item-tarif-wrap");
	itemTarif.each(function(){
		var itemHeight = parseInt($(this).height());
		if (itemHeight > minHeight){
			minHeight = itemHeight;
		}
	});
	itemTarif.height(minHeight + 80);
};
itemTarifAutoheight();
$(window).resize(function(){
	itemTarifAutoheight;
});

/*slider-testimonials*/
$(".slider-testimonials").slick({		 
	slidesToShow: 1,	   
	slidesToScroll: 1,
	arrows: true,
	nextArrow: '<div class="arrow-next"></div>',
	prevArrow: '<div class="arrow-prev"></div>',

});


















