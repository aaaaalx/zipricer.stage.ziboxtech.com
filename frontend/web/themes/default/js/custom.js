$(document).ready(function () {
    //checkbox switcher
    $('.btn-status-switch').on('click', function () {
        var $this = $(this);
        var target = $($this).data('for');
        var input = $('#'+target);
        if($this.hasClass('left')){
            if(parseInt(input.val()) != parseInt($this.data('value'))){
                input.val($this.data('value'));
            }
            else{
                var rightButtonValue = $('.btn-status-switch.right[data-for="'+target+'"]');
                rightButtonValue = rightButtonValue.length?rightButtonValue.data('value'):null;
                input.val(rightButtonValue);
            }
        }
        else if($this.hasClass('right')){
            input.val($this.data('value'));
        }
    });

     var chartsList = {};
     function setCharts(el, data, label) {
         "use strict";
         chartsList[el] = new Morris.Line({
             element: el,
             resize: true,
             data: data,
             xkey: 'year',
             ykeys: ['price'],
             labels: [label],
             lineColors: ['#3c8dbc'],
             hideHover: 'auto',
             parseTime: false,
         });
     }
     function initCharts(data) {
         if(data){
             if(Object.keys(data).length){
                 Object.keys(data).map(function(objectKey, index) {
                     var value = data[objectKey];
                     var label = 'Price';
                     setCharts('line-chart-'+objectKey, value, label);
                 });
             }
         }
     }
     if(window.chartsData){
         initCharts(chartsData);
     }

     $('.box-notification button[data-widget="remove"]').on('click', function(){
         $.ajax({
             url: '/notifications/delete?id='+$(this).data('id'),
             type: 'post',
             success: function (data) {

             }});
     });

    var a = {one:1,two:2,three:3,four:4};
    delete a.one;
    console.log(a);

     var b = [1,2,3,4];
     delete b[0];
     console.log(b);

});