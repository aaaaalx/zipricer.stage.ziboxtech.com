$(window).on('load', function() { // makes sure the whole site is loaded   
  $('.preloader').delay(1000).fadeOut('slow'); // will fade out the white DIV that covers the website. 
  setTimeout(function(){
    $("table.table-project-links>tbody>tr:odd").css("display", "none");
  }, 100);

});

/*char counter*/
$(".char-textarea").on("keyup",function(event){
  checkTextAreaMaxLength(this,event);
});

/*
Checks the MaxLength of the Textarea
-----------------------------------------------------
@prerequisite:  textBox = textarea dom element
        e = textarea event
                length = Max length of characters
                */

function checkTextAreaMaxLength(textBox, e) { 

var maxLength = parseInt($(textBox).data("length"));


if (!checkSpecialKeys(e)) { 
if (textBox.value.length > maxLength - 1) textBox.value = textBox.value.substring(0, maxLength); 
} 
$(".char-count").html(maxLength - textBox.value.length);

return true; 
}; 
/*
Checks if the keyCode pressed is inside special chars
-------------------------------------------------------
@prerequisite:  e = e.keyCode object for the key pressed
*/
function checkSpecialKeys(e) { 
  if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40) 
    return false; 
  else 
    return true; 
};
/*char counter*/

$(".slider-tarifs").slick({
  centerMode: true,  
  slidesToShow: 1,
  centerPadding: '33.3333%',
  arrows: false,
  dots: true,
  responsive: [
  {
    breakpoint: 1200,
    settings: {
      slidesToShow: 2, 
      centerMode: false,
      centerPadding: false,       
    }
  },
  {
    breakpoint: 992,
    settings: {
      slidesToShow: 1,
      
    }
  },
  ],
});



/*autoHeight slider tarifs items*/
function itemTarifAutoheight(){
  var minHeight = 0,
  itemTarif = $(".slider-tarifs .item-tarif-wrap");
  itemTarif.each(function(){
    var itemHeight = parseInt($(this).height());
    if (itemHeight > minHeight){
      minHeight = itemHeight;
    }
  });
  itemTarif.height(minHeight + 80);
};
itemTarifAutoheight();
$(window).resize(function(){
  itemTarifAutoheight;
});

/*project status icon*/
function projectStatus(){
  var projectStatus = $(".project-status");
  projectStatus.each(function(){
    if($(this).hasClass("active")){
      $(this).append('<i class="fa fa-check-circle fa-check-circle-own"></i><span>active</span>');
    }
    else {
      $(this).append('<i class="fa  fa-circle  fa-circle-own"></i><span>disabled</span>');
    };
  })

};
projectStatus();

/*show project-info*/
$(".show-project-info").click(function(){
  var btnParent = $(this).parent();
  btnParent.parent().next().slideToggle();
});

/*show empty-project-message*/
if($("table").is(".table-project-links")){
  $(".empty-project-message").css("display", "none");
  $(".preloader").css("display", "block");

}
else {
 $(".empty-project-message").css("display", "block"); 
 $(".preloader").css("display", "none");
}

/*col-in.parent() border responsive*/
function respBorder(){
  var windowWidth = window.innerWidth;
  var borderedBox = $(".col-in").parent();
  if (windowWidth > 767 && windowWidth <= 1200)  {
    borderedBox.eq(1).removeClass("grey-brd-resp");
  }
  else {
    borderedBox.eq(1).addClass("grey-brd-resp");
  }
};
respBorder();
$(window).resize(function(){
    respBorder();
  });

/*page create-project button status switch*/
$(".btn-status-switch.left").click(function(){
  $(this).toggleClass("unchecked").toggleClass("checked");
});
$(".btn-status-switch.right").click(function(){
  if ($(".btn-status-switch.left").hasClass("checked")){
    $(".btn-status-switch.left").toggleClass("unchecked").toggleClass("checked");
  }
});


/*file image preview create-project.html*/

function readURL(fileImg) {
     for(var i =0; i< fileImg.files.length; i++){
         if (fileImg.files[i]) {
            var reader = new FileReader();

            reader.onload = function (e) {
               var img = $('<img class="img-preview">');             
               img.attr('src', e.target.result);
               $(".preview-project-img").children().remove();
               img.appendTo('.preview-project-img');  
               img.parent().append('<div class="close-img-button"></div>');
              
            };
            reader.readAsDataURL(fileImg.files[i]);
           }
        }
    }

    $(".file-img").change(function(){
        readURL(this);

    });

$(document).on('click', '.close-img-button', function(){


    var imgCont = $(this).parent();
    var parent = imgCont.parents('.form-group');
    var input = parent.find('input[type="file"]').val('');
    var id = imgCont.data('id');

    console.log(imgCont);
    console.log(parent);
    console.log(input);
    console.log(id);

    if(id){
        $.ajax({
            url: '/projects/remove-image?id='+id,
            type: 'post',
            success: function (data) {

            }});
    }


  var item = $(this).parent().find(".img-preview");  
  item.remove();
  $(this).remove();

 
});

/*fila avatar preview profile.html*/
function readURLAvatar(fileImg) {
     for(var i =0; i< fileImg.files.length; i++){
         if (fileImg.files[i]) {
            var reader = new FileReader();

            reader.onload = function (e) {
               var img = $('<img class="img-preview">');             
               img.attr('src', e.target.result);
               $(".preview-avatar-img").children().remove();
               img.appendTo('.preview-avatar-img');                             
              
            }
            reader.readAsDataURL(fileImg.files[i]);
           }
        }
    }

    $(".file-avatar").change(function(){
        readURLAvatar(this);

    });





















