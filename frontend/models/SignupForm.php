<?php
namespace frontend\models;

use common\helpers\MainHelper;
use Yii;
use yii\base\Model;
use common\models\Users;
use common\models\Profile;
use yii\web\UploadedFile;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $referral_user_id;
    public $referral_link;
    public $status;
    public $icon;
    private $user;

    public function __construct(Profile $profile = null, array $config = [])
    {
        parent::__construct($config);
        if($profile){
            $this->user = $profile;
            $this->id = $profile->id;
            $this->username = $profile->username;
            $this->email = $profile->email;
            $this->referral_user_id = $profile->referral_user_id;
            $this->referral_link = $profile->referral_link;
            $this->status = $profile->status;
            $this->icon = $profile->icon;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\Profile', 'message' => 'This username has already been taken.', 'filter' => $this->user ? ['<>', 'username', $this->user->username]:null],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\Profile', 'message' => 'This email address has already been taken.', 'filter' => $this->user ? ['<>', 'email', $this->user->email]:null],

            [['password', 'password_repeat'], 'required', 'on'=>'userCreate'],
            ['password', 'string', 'min' => 6],

            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],

            [['password', 'password_repeat'], 'required', 'when' => function($model){
                return $model->password?$model->password!==$model->password_repeat:false;
            }, 'whenClient' => "function (attribute, value) {
                    return $('#signupform-password').val()?$('#signupform-password').val() !== $('#signupform-password_repeat').val():false;
                }"
            ],

            [['status', 'referral_user_id'], 'integer'],
            ['referral_link', 'string', 'min' => 5, 'max' => 32],

            ['icon', 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'minWidth' => 100, 'maxWidth' => 800, 'minHeight' => 100, 'maxHeight' => 800,],
        ];
    }

    public function setReferralUser($code){
        if($code){
            $referral_user = Profile::find()->where('referral_link = :referral',[':referral', $code])->one();
            if(!empty($referral_user)){
                $this->referral_user_id = $referral_user->id;
            }
        }
    }

    /**
     * Signs user up.
     *
     * @return Profile|null the saved model or null if saving fails
     */
    public function signup($user=false)
    {
        if (!$this->validate()) {
            return null;
        }
        if(!$user){
            $user = new Users();
            $user->edit($this->email, $this->password, true);
        }
        if($user->save()){
            $profile = new Profile();
            if($profile->edit($this->username, $this->email, $this->status, $this->referral_user_id, $profile->generateReferralCode(), $user->user_id, true) && $profile->save()){
                return $profile;
            }
        }
        return null;
    }

    public function uploadFile($path){
        $file = MainHelper::setUniqueFileName($this->icon->baseName, $this->icon->extension);
        if(!is_dir($path)){
            mkdir($path, 0777, true);
        }
        if($this->icon->saveAs($path.$file)){
            return $file;
        }
        return false;
    }

    public function deleteFiles($name=false){
        $path = $this->getUploadsPath();
        if(is_dir($path)){
            if(file_exists($path.$name)){
                unlink($path.$name);
                MainHelper::remove_if_dir_empty($path);
            }
            else{
                MainHelper::remove_dir($path);
            }
        }
    }

    public function getUploadsPath(){
        return Yii::getAlias('@uploads').'\profiles\\'.$this->id.'\\';
    }

    public function isFileAdded(){
        return $this->icon instanceof UploadedFile;
    }

    public function getUploadsUrl($dir, $fileName){
        return Yii::$app->params['frontendHost'].'uploads/profiles/'.$dir.'/'.$fileName;
    }

}
