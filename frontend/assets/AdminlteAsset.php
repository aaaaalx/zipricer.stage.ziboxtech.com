<?php

namespace frontend\assets;
use yii\web\AssetBundle;

class AdminlteAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/dist';

    public $css = [
        'css/AdminLTE.min.css',
        'css/skins/_all-skins.min.css',
    ];

    public $js = [
        "js/adminlte.min.js",
        "js/demo.js",
    ];
}