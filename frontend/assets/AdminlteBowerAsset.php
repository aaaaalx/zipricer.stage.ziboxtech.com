<?php

namespace frontend\assets;
use yii\web\AssetBundle;

class AdminlteBowerAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/bower_components';

    public $css = [
        'bootstrap/dist/css/bootstrap.min.css',
        'font-awesome/css/font-awesome.min.css',
        'Ionicons/css/ionicons.min.css',
        'morris.js/morris.css',
        'jvectormap/jquery-jvectormap.css',
        'bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        'bootstrap-daterangepicker/daterangepicker.css'
    ];

    public $js = [
        "jquery-sparkline/dist/jquery.sparkline.min.js",
        "jquery-knob/dist/jquery.knob.min.js",
        "moment/min/moment.min.js",
        "bootstrap-daterangepicker/daterangepicker.js",
        "bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
        "jquery-slimscroll/jquery.slimscroll.min.js",
        "fastclick/lib/fastclick.js",
        "raphael/raphael.min.js",
        "morris.js/morris.min.js"
    ];
}