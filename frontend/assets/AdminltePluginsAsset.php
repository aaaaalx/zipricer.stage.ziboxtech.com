<?php

namespace frontend\assets;
use yii\web\AssetBundle;

class AdminltePluginsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins';

    public $css = [
        'bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
    ];

    public $js = [
        "jvectormap/jquery-jvectormap-1.2.2.min.js",
        "jvectormap/jquery-jvectormap-world-mill-en.js",
        "bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js",
    ];
}