<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'themes/default/css/style.css',
        'themes/default/css/custom.css'
    ];
    public $js = [
        'themes/default/libs/js/bootstrap.js',
        'themes/default/libs/js/slick.js',
        'themes/default/js/main.js',
        'themes/default/js/custom.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'frontend\assets\AdminlteAsset',
        'frontend\assets\AdminlteBowerAsset',
        'frontend\assets\AdminltePluginsAsset'
    ];
}
